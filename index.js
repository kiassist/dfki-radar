export var Radar = function(){

	// private variables:
	var $Private = new Map();

	// static private variables:
	var $StaticPrivate = {};

	/*----------------------------------------
	public fields:
		this.foo = 123;

	public globals:
		Radar.foo = 123;

	public methods:
		foo(args...) { ... }
		this.foo(args...);

	public functions:
		Radar.foo(args...) { ... }
		Radar.foo(args...);

	private fields:
		$Private[this].foo = 123;

	private globals:
		$StaticPrivate.foo = 123;

	private methods:
		$StaticPrivate.foo = function(self, args...) { ... }
		$StaticPrivate.foo(this, args...);

	private functions:
		$StaticPrivate.foo = function(args...) { ... }
		$StaticPrivate.foo(args...);
	----------------------------------------*/

	class Radar {
		constructor(src, parent) {
			// TODO: is (src == 'none') ...
			var self = this;
			$Private[this] = {};
			var pvars = $Private[this];

			pvars.destroyed = false;

			pvars.parent = parent;
			pvars.data = [];

			pvars.canvas = null;
			pvars.ctx = null;
			pvars.contentDiv = null;

			pvars.ready = false;
			pvars.readyTime = 0;
			var jobs = 0;

			pvars.jobsDone = false;

			pvars.configSet = false;
			pvars.datasetSet = false;
			pvars.loadDataHelper = null;



			pvars.incJobs = function() {
				jobs++;
			}
			pvars.decJobs = function() {
				if (jobs>0) jobs--;
				if (jobs === 0) {
					pvars.jobsDone = true;
					$StaticPrivate.onready(self);
				}

			}

			pvars.incJobs();

			pvars.errorState = false;
			pvars.err = function(msg) {
				pvars.errorState = true;
				console.error(msg);
			}


			pvars.id = undefined;
			if(parent.hasAttribute('id')){
				if (parent.attributes['id'] in $StaticPrivate.idDict) {
					console.warn('Duplicate id "'+parent.attributes['id']+'"');
					return;
				}
				pvars.id = parent.getAttribute('id');
				$StaticPrivate.idDict[ pvars.id ] = this;
			}

			$StaticPrivate.radarList.push(this);

			src = src.trim();
			if (src != 'none') {
				this.setConfig(src);
			}
		}

		get ready() {
			return $Private[this].ready;
		}

		get errorState() {
			return $Private[this].errorState;
		}

		destroy() {
			var pvars = $Private[this];
			if (pvars.destroyed) return;

			pvars.destroyed = true;
			pvars.parent.remove();

			var index = $StaticPrivate.radarList.indexOf(this);
			if (index !== -1) $StaticPrivate.radarList = $StaticPrivate.radarList.splice(index, 1);


			if (pvars.id != undefined) delete $StaticPrivate.idDict[pvars.id];



			delete $Private[this];
		}

		setConfig(src) {
			var self = this;
			var pvars = $Private[this];
			if(pvars.configSet) return;
			pvars.configSet = true;
			if (typeof src === 'object' && src !== null) {
				$StaticPrivate.init( self, src );
			}
			else if (src.startsWith('{')) {
				$StaticPrivate.init( self, JSON.parse(src) );
			}
			else {
				pvars.incJobs();
				$StaticPrivate.loadJSON(
					src,
					function(json){
						$StaticPrivate.init(self, json)
						pvars.decJobs();
					},
					function(){
						pvars.err('Could not load radar settings from file "'+src+'"');
						pvars.decJobs();
					}
				);
			}
			if (pvars.loadDataHelper) {
				this.setData(pvars.loadDataHelper);
				pvars.loadDataHelper = null;
			}
		}

		setData(src) {
			var pvars = $Private[this];
			if (!pvars.configSet) {
				pvars.loadDataHelper = src;
				return;
			}
			$StaticPrivate.setData(this, src);
		}

		setView(id) {
			var pvars = $Private[this];
			if (pvars.destroyed) return;

			var lastView = pvars.currentView;
			if (id in pvars.views) pvars.currentView = pvars.views[id];

			$StaticPrivate.lookAt(this, undefined);
			pvars.selectedDataElementID = undefined;
			pvars.siblings = [];
			$StaticPrivate.filterDataElement( this, pvars.selectedDataElementID, pvars.searchBox.value );
			$StaticPrivate.setViewSwitchAni( this, lastView, pvars.currentView );
			if (pvars.searchBox.value == '') $StaticPrivate.HideSideDiv( this );

			if (lastView) lastView.legendDiv.style.display = 'none';
			if (pvars.settings.show_legend_box) pvars.currentView.legendDiv.style.display = 'block';

			lastView.descriptionDiv.style.display = 'none';
			pvars.currentView.descriptionDiv.style.display = 'block';

			if (lastView.link) lastView.link.classList.remove('radar-views-box-name-selected');
			if (pvars.currentView.link) pvars.currentView.link.classList.add('radar-views-box-name-selected');

			$StaticPrivate.updateLinks(this, pvars.currentView);

			this.requestRedraw();
		}

		requestRedraw() {
			var pvars = $Private[this];
			pvars.redraw = true;
		}
	}

	// this stores private global variables:
	$StaticPrivate.idDict = {};

	// list with all radars:
	$StaticPrivate.radarList = [];

	$StaticPrivate.startTime = (new Date()).getTime();
	$StaticPrivate.msecs = 0;

	$StaticPrivate.escapeHTML = function(str){
		return str.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
	}


	$StaticPrivate.iconMap = {
		'SYMBOL_CIRCLE' : 'A',
		'SYMBOL_TRIANGLE' : 'B',
		'SYMBOL_SQUARE' : 'C',
		'SYMBOL_DIAMOND' : 'D',
		'SYMBOL_PENTAGON' : 'E',
		'SYMBOL_HEXAGON' : 'F',
		'SYMBOL_TREFOIL' : 'G',
		'SYMBOL_QUATREFOIL' : 'H',
		'SYMBOL_STAR' : 'I',
		'SYMBOL_HEART' : 'J',
		'SYMBOL_CURVED_TRIANGLE' : 'K',
		'SYMBOL_TRIANGLE_2' : 'L',
		'SYMBOL_PENTAGON_2' : 'M',
		'SYMBOL_HEXAGON_2' : 'N',
		'SYMBOL_CURVED_TRIANGLE_2' : 'O',
		'SYMBOL_OCTAGON' : 'P',
		'ICON_RADAR' : 'a',
		'ICON_CIRCLE' : 'b',
		'ICON_SQUARE' : 'c',
		'ICON_TRIANGLE' : 'd',
		'ICON_STAR' : 'e',
		'ICON_HEART' : 'f',
		'ICON_PIN' : 'g',
		'ICON_CROSSHAIR' : 'h',
		'ICON_BOOKMARK' : 'i',
		'ICON_FLAG' : 'j',
		'ICON_SEARCH' : 'k',
		'ICON_LIGHTBULB' : 'l',
		'ICON_PAPERCLIP' : 'm',
		'ICON_FILE' : 'n',
		'ICON_FOLDER' : 'o',
		'ICON_BOOK' : 'p',
		'ICON_SUITCASE' : 'q',
		'ICON_CALENDAR' : 'r',
		'ICON_MAIL' : 's',
		'ICON_AT' : 't',
		'ICON_HOME' : 'u',
		'ICON_APPLICATION' : 'v',
		'ICON_MONITOR' : 'w',
		'ICON_DEVICE_DESKTOP' : 'w',
		'ICON_DEVICE_NOTEBOOK' : 'x',
		'ICON_DEVICE_MOBILE' : 'y',
		'ICON_USER' : 'z',
		'ICON_USERS' : 'Q',
		'ICON_SPEECH_BUBBLE' : 'R',
		'ICON_CHART' : 'S',
		'ICON_LAB_FLASK' : 'T',
		'ICON_EYE' : 'U',
		'ICON_TARGET' : 'V'
	};
	$StaticPrivate.getIconString = function(iconID, defaultID=undefined) {
		return iconID in $StaticPrivate.iconMap ? $StaticPrivate.iconMap[iconID] : (defaultID ? $StaticPrivate.getIconString(defaultID,'ICON_RADAR') : 'A');
	}

	$StaticPrivate.loadJSON = function(url, cb, errcb=null) {
		var xhr = new XMLHttpRequest();
		xhr.open("GET", url, true);
		xhr.onreadystatechange = function() {
			if (xhr.status != 200 || xhr.readyState != 4) return;
			cb(JSON.parse(xhr.responseText));
		}
		xhr.onerror = function() {
			if (typeof errcb === "function") errcb();
		}
		xhr.send();
	}

	$StaticPrivate.selectDataElement = function(self, elemID) {
		var pvars = $Private[self];
		if (pvars.destroyed) return;

		if (elemID == undefined || elemID<0 || elemID>=pvars.currentView._data.length) {
			pvars.selectedDataElementID = undefined;
			pvars.siblings = [];
			$StaticPrivate.lookAt(self, elemID);

			if (pvars.searchBox.value == '') $StaticPrivate.HideSideDiv(self);
		}
		else {
			pvars.selectedDataElementID = elemID;
			pvars.siblings = [];

			// get list of siblings:
			var currentName = pvars.data[pvars.selectedDataElementID][pvars.currentView.fields.name];
			for(var i=0; i<pvars.data.length; ++i) {
				if (pvars.data[i][pvars.currentView.fields.name] == currentName) pvars.siblings.push(i);
			}

			$StaticPrivate.lookAt(self, elemID);

			$StaticPrivate.ShowSideDiv(self);
		}
	}

	$StaticPrivate.onready = function(self) {
		var pvars = $Private[self];
		if (!pvars.jobsDone || pvars.ready) return;

		// TODO: put this stuff in an other function so that views can be added on the fly ...
		var isFirstView = true;
		var createItemLink = function(view, title, id) {
			return $StaticPrivate.createLink(
				title,
				function(){
					$StaticPrivate.lookAtFast(self, id);
					var e = view._data[id];
					e.focus = true;
					pvars.hoverDataElementID = id;
					self.requestRedraw();
				},
				function(){
					var e = view._data[id];
					e.focus = false;
					self.requestRedraw();
				},
				function(){
					$StaticPrivate.selectDataElement(self, id);
					pvars.currentView.updateDescriptionBox();
					self.requestRedraw();
					var el = document.getElementById("_A"+id);
					el.scrollIntoView();
				}
			)
		}
		for(const vid in pvars.views) {
			var view = pvars.views[vid];
			view._data = [];
			for(var i=0; i<pvars.data.length; ++i) {
				view._data.push({
					visible : 0.0,
					color : '#000',
					x : 0.0,
					y : 0.0,
					ptr : i,
					screenPosX : 0.0,
					screenPosY : 0.0,
					screenRadius : 0.0,
					filter : true,

					visibleNumber : 0,
					link : createItemLink(view, pvars.data[i][view.fields.name], i),
					focus : false
				});
				pvars.showLink(view._data[view._data.length-1].link);
			}

			var sections = {}; //map: section_name -> (map: ring_name -> array_of_data_elements)
			var ring_names = [];
			var classes = {}; //map: class_name -> (map: class_name -> array_of_data_elements)

			// get section names, ring names and class names:
			for(var i=0; i<pvars.data.length; ++i) {
				var e = pvars.data[i];

				if( !( e[view.fields.section] in sections ) ) sections[e[view.fields.section]] = {};
				if( !( ring_names.includes(e[view.fields.ring]) ) ) ring_names.push(e[view.fields.ring]);
				if( !( e[view.fields['class']] in classes ) ) classes[e[view.fields['class']]] = {};
			}
			// create entries for rings in each section:
			for(const sid in sections) {
				for(var i=0; i<ring_names.length; ++i) {
					sections[sid][ring_names[i]] = [];
				}
			}
			// get list of elements for each ring-segment of each section:
			for(var i=0; i<pvars.data.length; ++i) {
				var e = pvars.data[i];

				var section = sections[e[view.fields.section]];
				section[e[view.fields.ring]].push(i);
			}
			// for all rings get the maximum number of elements within a section:
			var ring_max_elements = {};
			for(var i=0; i<ring_names.length; ++i) {
				ring_max_elements[ring_names[i]] = 0;
				for (const sid in sections) {
					if (sections[sid][ring_names[i]].length > ring_max_elements[ring_names[i]])
						ring_max_elements[ring_names[i]] = sections[sid][ring_names[i]].length;
				}
			}

			// get section order:
			if (view.section_order == 'AUTO') {
				view.section_order = [];
				for(const sid in sections) {
					view.section_order.push(sid);
				}
				view.section_order.sort((a, b) => a.localeCompare(b, undefined, {sensitivity: 'base'}));
			}
			else {
				var temp = [];
				for (var i=0; i<view.section_order.length; ++i) {
					if (view.section_order[i] in sections) temp.push(view.section_order[i]);
				}
				view.section_order = temp;
			}

			// get ring order:
			if (view.ring_order == 'AUTO') {
				view.ring_order = [];
				view.ring_order = ring_names.slice();
				view.ring_order.sort((a, b) => a.localeCompare(b, undefined, {sensitivity: 'base'}));
			}
			else {
				var temp = [];
				for (var i=0; i<view.ring_order.length; ++i) {
					if (ring_names.includes(view.ring_order[i])) temp.push(view.ring_order[i]);
				}
				view.ring_order = temp;
			}

			// get class order:
			if (view.class_order == 'AUTO') {
				view.class_order = [];
				for(const id in classes) {
					view.class_order.push(id);
				}
				view.class_order.sort((a, b) => a.localeCompare(b, undefined, {sensitivity: 'base'}));
			}
			else {
				var temp = [];
				for (var i=0; i<view.class_order.length; ++i) {
					if (view.class_order[i] in classes) temp.push(view.class_order[i]);
				}
				view.class_order = temp;
			}
			// create legend:
			view.legendDiv = document.createElement('div');
			if (pvars.settings.show_legend_box) {
				view.legendDiv.style.display = isFirstView ? 'block' : 'none';
			}
			else {
				view.legendDiv.style.display = 'none';
			}
			for (var i=0; i<view.class_order.length; ++i) {
				var link = document.createElement('span');
				link.innerHTML = '<div class="radar-icon">'+$StaticPrivate.getIconString(view.class_symbols[view.class_order[i]])+'</div>&nbsp;&nbsp;'+$StaticPrivate.escapeHTML(view.class_names[view.class_order[i]]);
				view.legendDiv.appendChild(link);
			}
			pvars.viewLegendDiv.appendChild(view.legendDiv);

			// create description div:
			view.descriptionSections = [];
			view.descriptionDiv = document.createElement('div');
			view.descriptionDiv.classList.add("radar-description-list");
			var ddstyle = window.getComputedStyle(pvars.searchBox);
			function createDescriptionMouseFuncs(el) {
				el.hlDiv.onmouseover = function() {
					pvars.hoverDataElementID = el.index;
					self.requestRedraw();
				}
				el.hlDiv.onclick = function() {
					$StaticPrivate.selectDataElement(self, el.index);
					pvars.currentView.updateDescriptionBox();
					self.requestRedraw();
				}
			}
			for (var i=0; i<view.ring_order.length; ++i) {
				var descrSec = document.createElement('div');
				var elements = [];
				var r = view.ring_order[i];
				descrSec.innerHTML = '<h1>'+view.ring_names[r]+'</h1>';
				view.descriptionDiv.appendChild(descrSec);
				for (var k=0; k<view._data.length; ++k) {
					var e = view._data[k];
					var d = pvars.data[e.ptr];
					if (d[view.fields.ring] != r) continue;
					var elementDiv = document.createElement('div');
					elementDiv.classList.add("radar-description-box");
					elementDiv.innerHTML = '<h2 id="_A'+k+'">'
						+'<div class="radar-icon" style="color:'+view.colors[d[view.fields.color]]+';">'+$StaticPrivate.getIconString(view.class_symbols[d[view.fields['class']]])+'</div>&nbsp;&nbsp;'
						+(e.ptr+1)+' – '+$StaticPrivate.escapeHTML(d[view.fields.name] )+'</h2>';
					var descriptionDiv = document.createElement('div');
					descriptionDiv.classList.add("radar-description-text");
					descriptionDiv.innerHTML = d[view.fields.text];
					elementDiv.appendChild(descriptionDiv);
					view.descriptionDiv.appendChild(elementDiv);
					descriptionDiv.style.display = 'none';
					var el = {
						index : k,
						hlDiv : elementDiv,
						descriptionDiv : descriptionDiv
					};
					createDescriptionMouseFuncs(el);
					elements.push(el);
				}
				view.descriptionSections.push({
					div : descrSec,
					elements : elements
				});
			}
			view.descriptionDiv.style.display = isFirstView ? 'block' : 'none';
			pvars.sideDiv.appendChild(view.descriptionDiv);
			view.updateDescriptionBox = function() {
				for (var i=0; i<pvars.currentView.descriptionSections.length; ++i) {
					var elemCount = 0;
					var sec = pvars.currentView.descriptionSections[i];
					for (var k=0; k<sec.elements.length; ++k) {
						var el = sec.elements[k];
						if ( (pvars.selectedDataElementID!=undefined || pvars.searchBox.value!='')
						     && (pvars.currentView._data[el.index].filter || pvars.selectedDataElementID==el.index)
						     && pvars.currentView.section_order.includes(pvars.data[pvars.currentView._data[el.index].ptr][pvars.currentView.fields.section])
						   ) {
							elemCount++;
							el.hlDiv.style.display = 'block';
							el.descriptionDiv.style.display = el.index == pvars.selectedDataElementID ? 'block' : 'none';
							if (el.index == pvars.selectedDataElementID) el.hlDiv.scrollIntoView();
						}
						else {
							el.hlDiv.style.display = 'none';
						}
					}
					sec.div.style.display = elemCount>0 ? 'block' : 'none';
				}
			}





			// calculate radii:
			var radii = [];
			for (var i=0; i<view.ring_order.length; ++i) {
				var A = ring_max_elements[view.ring_order[i]];
				if (i==0) {
					radii.push( Math.sqrt(A/(Math.PI/view.section_order.length)) );
				}
				else {
					var r1 = radii[radii.length-1];
					radii.push( Math.sqrt(A/(Math.PI/view.section_order.length) + r1*r1) );
				}
			}
			var rmax = radii[radii.length-1];
			for (var i=0; i<radii.length; ++i) {
				radii[i] /= rmax;
			}

			var rw = 0.75;
			for (var i=0; i<radii.length; ++i) {
				radii[i] = (radii[i]+((i+1)/radii.length)*rw)/(1+rw);
			}
			view._radii = radii;

			// position elements (and set colors):
			var secs = view.section_order.length;
			for (var i=0; i<view._data.length; ++i) {
				var e = view._data[i];
				var d = pvars.data[e.ptr];
				var s = view.section_order.indexOf(d[view.fields.section]);
				var r = view.ring_order.indexOf(d[view.fields.ring]);
				if (s>=0 && r>=0) {
					e.visible = 1.0;
					var rx = 2*Math.PI * (s + (Math.random()*0.98+0.01)) / secs + view.rotation;
					var ry = (r==0?0:view._radii[r-1]) + (view._radii[r]-(r==0?0:view._radii[r-1]))*(Math.random()*0.98+0.01);
					e.x = Math.cos(rx)*ry;
					e.y = Math.sin(rx)*ry;
					e.color = d[view.fields.color] in view.colors ? view.colors[ d[view.fields.color] ] : '#F00';
				}
			}

			// position elements using spiral:
			//     usage: segElems[segment][ring] -> list of elements
			var segElems = [];
			for (var s=0; s<secs; ++s) {
				segElems[s] = [];
				for (var r=0; r<view.ring_order.length; ++r) {
					segElems[s][r] = [];
				}
			}
			for (var i=0; i<view._data.length; ++i) {
				var e = view._data[i];
				var d = pvars.data[e.ptr];
				var s = view.section_order.indexOf(d[view.fields.section]);
				var r = view.ring_order.indexOf(d[view.fields.ring]);
				if (s>=0 && r>=0) {
					segElems[s][r].push(e);
				}
			}

			// see: https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
			//     ==> Line defined by two points
			function pointLineDist(px,py,lx0,ly0,lx1,ly1) {
				return Math.abs( (ly1-ly0)*px - (lx1-lx0)*py + lx1*ly0 - ly1*lx0 ) / Math.sqrt( (ly1-ly0)*(ly1-ly0) + (lx1-lx0)*(lx1-lx0) );
			}
			// for lines starting at 0:
			function pointLineDist2(px,py,lx,ly) {
				return Math.abs( ly*px - lx*py ) / Math.sqrt( ly*ly + lx*lx );
			}
			//----------------------------------------

			for (var r=0; r<view.ring_order.length; ++r) {
				var r0 = (r==0?0.05:view._radii[r-1])+0.03;
				var r1 = view._radii[r]-0.03;
				for (var s=0; s<secs; ++s) {
					if (segElems[s][r].length == 0) continue;
					var b0x = Math.cos(2*Math.PI*s/secs);
					var b0y = Math.sin(2*Math.PI*s/secs);
					var b1x = Math.cos(2*Math.PI*(s+1)/secs);
					var b1y = Math.sin(2*Math.PI*(s+1)/secs);

					var alpha0 = 2*Math.PI * s / secs;
					var alpha1 = 2*Math.PI * (s+1) / secs;

					var more = 5;
					var maxRounds = 1000;
					while(true) {
						maxRounds--;
						if (maxRounds<0) {
							pvars.err('Cannot place elements on radar ...');
							break;
						}
						var c = Math.sqrt( (r1*r1-r0*r0)/((segElems[s][r].length+more)*secs) );
						var n0 = (r0/c)*(r0/c);
						var n1 = (r1/c)*(r1/c);
						var locations = [];

						for (var n=n0; n<=n1; ++n) {
							var theta = n*2.399967; // = n * 137.508/180*Math.PI;
							if ( theta%(2*Math.PI) > alpha0 && theta%(2*Math.PI) < alpha1 ) {
								var er = c*Math.sqrt(n);
								var ex = Math.cos(theta /*+ view.rotation*/)*er;
								var ey = Math.sin(theta /*+ view.rotation*/)*er;
								var exr = Math.cos(theta + view.rotation)*er;
								var eyr = Math.sin(theta + view.rotation)*er;
								if (pointLineDist2(ex,ey,b0x,b0y)>0.03 && pointLineDist2(ex,ey,b1x,b1y)>0.03) {
									locations.push( {x:exr, y:eyr} );
								}
							}
						}
						if (locations.length < segElems[s][r].length) {
							more += 5;
						}
						else {
							for (var ei=0; ei<segElems[s][r].length; ++ei) {
								var e = segElems[s][r][ei];
								e.x = locations[ei].x;
								e.y = locations[ei].y;
							}
							break;
						}
					}
				}
			}

			// get best angle for ring labels:
			view._ring_label_rotation = 999.9;
			var bestAngle = 999;
			for (var i=0; i<secs; ++i) {
				var ca = 2*Math.PI * i/secs + view.rotation;
				var angle = Math.abs( Math.atan2(Math.sin(ca), Math.cos(ca)) - Math.atan2(0, 1) );
				if(angle < bestAngle) {
					bestAngle = angle;
					view._ring_label_rotation = ca;
				}
			}

			isFirstView = false;
		}

		self.setView(pvars.currentView.id);

		pvars.ready = true;
		pvars.readyTime = $StaticPrivate.msecs;
		// TODO: emit some signal
	}

	$StaticPrivate.setData = function(self, dataset) {
		var pvars = $Private[self];
		if (pvars.datasetSet) return;
		pvars.datasetSet = true;
		if (typeof dataset === 'object' && dataset !== null) {
			pvars.data = dataset;
			pvars.decJobs();
		}
		else {
			var src = dataset.trim();
			$StaticPrivate.loadJSON(
				src,
				function(json){
					pvars.data = json;
					pvars.decJobs();
				},
				function(){
					pvars.err('Could not load dataset "'+src+'"');
				}
			);
		}
	}
	
	$StaticPrivate.init = function(self, settings) {
		var pvars = $Private[self];

		if (settings == undefined) settings = {}
		pvars.settings = Object.assign({}, $StaticPrivate.defaultSettings); // init settings with shallow copy of the default settings

		for (const key in settings) {
			pvars.settings[key] = settings[key]
		}

		$StaticPrivate.initRadarCSS(self);

		if ('error' in pvars.settings) {
			console.warn(pvars.settings.error);
		}
		pvars.incJobs();
		if ('dataset' in pvars.settings && typeof pvars.settings.dataset === "string") {
			$StaticPrivate.setData(self, pvars.settings.dataset)
		}

		if (typeof pvars.settings.width === "number") pvars.settings.width = pvars.settings.width+'px';
		if (typeof pvars.settings.width !== "string") pvars.settings.width = "800px";
		pvars.parent.style.width = pvars.settings.width;

		if (typeof pvars.settings.height === "number") pvars.settings.height = pvars.settings.height+'px';
		if (typeof pvars.settings.height !== "string") pvars.settings.height = "600px";
		pvars.parent.style.height = pvars.settings.height;

		var pixelWidth = pvars.parent.clientWidth;
		var pixelHeight = pvars.parent.clientHeight;

		pvars.aspectRatio = pixelWidth / pixelHeight;

		// correct resolution according to settings.maxres
		if (pixelWidth > pvars.settings.maxres) {
			pixelHeight = pixelHeight/pixelWidth*pvars.settings.maxres;
			pixelWidth = pvars.settings.maxres;
		}
		if (pixelHeight > pvars.settings.maxres) {
			pixelWidth = pixelWidth/pixelHeight*pvars.settings.maxres;
			pixelHeight = pvars.settings.maxres;
		}

		pvars.rescale = pixelWidth / pvars.parent.clientWidth;

		pvars.redraw = true;

		if (typeof pvars.settings.title !== 'string') pvars.settings.title = '?';

		pvars.canvas = document.createElement('canvas');
		var can = pvars.canvas;
		pvars.parent.style += `
			-moz-user-select: -moz-none;
			-khtml-user-select: none;
			-webkit-user-select: none;
			-o-user-select: none;
			user-select: none;
			width: ${pixelWidth};
			position: "relative";
		`
		can.width = pixelWidth;
		can.height = pixelHeight;
		can.style.position = "relative"
		can.style.width = pvars.settings.width;
		can.style.height = pvars.settings.height;
		can.style.left = "0px";
		can.style.top = "0px";
		can.style.zIndex = "1";

		pvars.sideDivVisible = false;
		pvars.sideDivClickable = true;
		pvars.sideDivHidingTime = -99999;
		pvars.sideDiv = document.createElement('div');
		pvars.sideDiv.classList.add("radar-sidebox");
		pvars.sideDiv.style =
		`
			position: absolute;
			right: 0px;
			display: flex;
			height: 100%;
			z-index: 3;
		`
		pvars.parent.appendChild(pvars.sideDiv);

		$StaticPrivate.updateSideDiv = function(self) {
			if (pvars.sideDivVisible && !pvars.sideDivClickable) {
				pvars.sideDivClickable = true;
				pvars.sideDiv.style.display = "block";
			}
			else if (!pvars.sideDivVisible && pvars.sideDivClickable && $StaticPrivate.msecs>pvars.sideDivHidingTime+300) {
				pvars.sideDivClickable = false;
				pvars.sideDiv.style.display = "none";
			}
		}
		$StaticPrivate.ShowSideDiv = function(self) {
			if (pvars.sideDivVisible == true) return;
			pvars.sideDivVisible = true;
			pvars.sideDiv.style.display = "block";
			pvars.sideDiv.classList.add("radar-sidebox-open");


			$StaticPrivate.setCorrectionOffset(self, -pvars.sideDiv.clientWidth*0.5*pvars.rescale, 0.0);
		}
		$StaticPrivate.HideSideDiv = function(self) {
			if (pvars.sideDivVisible == false) return;
			pvars.sideDivVisible = false;
			pvars.sideDiv.classList.remove("radar-sidebox-open");
			pvars.sideDivHidingTime = $StaticPrivate.msecs;

			$StaticPrivate.setCorrectionOffset(self, 0.0, 0.0);
		}
		$StaticPrivate.HideSideDiv(self);

		pvars.searchBoxDiv = document.createElement('div');
		pvars.searchBoxDiv.style =
		`
			width: `+(pvars.sideDiv.clientWidth)+`px;
			position: absolute;
			right: 0px;
			display: flex;
			z-index: 3;
		`
		pvars.parent.appendChild(pvars.searchBoxDiv);

		pvars.searchBoxSpan = document.createElement('div');
		pvars.searchBoxSpan.classList.add("radar-searchbox-container");
		pvars.searchBoxSpan.innerHTML = '<div class="radar-icon radar-searchbox-icon">'+$StaticPrivate.getIconString('ICON_SEARCH')+'</div>';
		pvars.searchBoxSpan.style =
		`
			right: 0px;
			display: flex;
			overflow: hidden;
		`
		pvars.searchBoxDiv.appendChild(pvars.searchBoxSpan);

		pvars.searchBox = document.createElement("input");
		pvars.searchBox.classList.add("radar-searchbox-input");
		pvars.searchBox.style =
		`
			z-index: 3;
		`
		pvars.searchBox.setAttribute('type', 'text');
		pvars.searchBox.setAttribute('placeholder', 'Filter...');
		pvars.searchBox.addEventListener('input', e => {
			$StaticPrivate.lookAt(this, undefined);
			pvars.selectedDataElementID = undefined;
			pvars.siblings = [];

			$StaticPrivate.filterDataElement(self, pvars.selectedDataElementID, e.target.value);
			if (e.target.value == '') {
				if (pvars.selectedDataElementID==undefined) $StaticPrivate.HideSideDiv(self);
			}
			else {
				$StaticPrivate.ShowSideDiv(self);
			}

			$StaticPrivate.updateLinks(self, pvars.currentView, false);
		});
		pvars.searchBoxSpan.appendChild(pvars.searchBox);

		pvars.viewSwitchDiv = document.createElement("div");
		pvars.viewSwitchDiv.classList.add("radar-views-box");
		pvars.viewSwitchDiv.style.display = pvars.settings.show_views_box ? "block" : "none";
		pvars.parent.appendChild(pvars.viewSwitchDiv);

		pvars.addViewLink = function(viewID) {
			if (!(viewID in pvars.views)) return;
			var view = pvars.views[viewID];

			var link = document.createElement("a");
			link.innerHTML = '<div class="radar-icon">'+$StaticPrivate.getIconString(view.icon, 'ICON_RADAR')+'</div>&nbsp;&nbsp;'+$StaticPrivate.escapeHTML(view.name);
			link.onclick = function() {
				self.setView(viewID);
			};
			link.href = "javascript:void(0)";
			pvars.viewSwitchDiv.appendChild(link);
			view.link = link;
		}

		pvars.viewLegendDiv = document.createElement("div");
		pvars.viewLegendDiv.classList.add("radar-legend-box");
		pvars.parent.appendChild(pvars.viewLegendDiv);

		// measure sizes:
		function CalculateDPI() {
			const el = document.createElement('div');
			el.style = 'width: 10in;'
			document.body.appendChild(el);
			const dpi = el.offsetWidth / 10.0;
			document.body.removeChild(el);

			return dpi;
		}
		Radar.DPI = CalculateDPI();
		Radar.DPCM = Radar.DPI/2.54;

		pvars.mouseDown = false;
		pvars.mouseScreenX = 0.0;
		pvars.mouseScreenY = 0.0;
		pvars.mouseRadarX = 0.0;
		pvars.mouseRadarY = 0.0;
		pvars.hoverDataElementID = undefined; // index of the element the mouse is hovering over
		pvars.offsetX = 0.5;
		pvars.offsetY = 0.5;
		pvars.dragOffsetX = pvars.offsetX;
		pvars.dragOffsetY = pvars.offsetY;
		pvars.dragStartX = 0.0;
		pvars.dragStartY = 0.0;
		pvars.dragStartPixelsX = 0.0;
		pvars.dragStartPixelsY = 0.0;
		pvars.dragPixelsMaxDist = 0.0;
		pvars.isDragging = false;
		pvars.zoom = 1.0;

		pvars.correctionOffset = {
			x : 0.0,
			y : 0.0,
			aniSrcX : 0.0,
			aniSrcY : 0.0,
			aniDstX : 0.0,
			aniDstY : 0.0,
			aniStartTime : -10000,
			aniEndTime : 0
		};

		pvars.viewSwitchAnimation = {
			aniStartTime : -10000,
			aniEndTime : 0,
			srcView : null,
			dstView : null,
			delta : 1.0
		}
		$StaticPrivate.updateViewSwitchAni = function(self) {
			var t = $StaticPrivate.msecs;
			if (t > pvars.viewSwitchAnimation.aniEndTime) {
				pvars.viewSwitchAnimation.delta = 1.0;
			}
			else {
				var t = pvars.viewSwitchAnimation.aniEndTime - pvars.viewSwitchAnimation.aniStartTime;
				var dt = $StaticPrivate.msecs - pvars.viewSwitchAnimation.aniStartTime;
				pvars.viewSwitchAnimation.delta = t > 0 ? $StaticPrivate.easingFunctions.easeInOutSine(dt/t) : 1.0;
			}

		}
		$StaticPrivate.setViewSwitchAni = function(self, srcView, dstView) {
			pvars.viewSwitchAnimation.aniStartTime = $StaticPrivate.msecs;
			pvars.viewSwitchAnimation.aniEndTime = pvars.viewSwitchAnimation.aniStartTime+300;
			pvars.viewSwitchAnimation.srcView = srcView;
			pvars.viewSwitchAnimation.dstView = dstView;
			pvars.viewSwitchAnimation.delta = 0.0;
		}

		$StaticPrivate.updateCorrectionOffset = function(self) {
			var t = $StaticPrivate.msecs;
			if (t > pvars.correctionOffset.aniEndTime) {
				pvars.correctionOffset.x = pvars.correctionOffset.aniDstX;
				pvars.correctionOffset.y = pvars.correctionOffset.aniDstY;
			}
			else {
				var t = pvars.correctionOffset.aniEndTime - pvars.correctionOffset.aniStartTime;
				var dt = $StaticPrivate.msecs - pvars.correctionOffset.aniStartTime;
				var d = t > 0 ? $StaticPrivate.easingFunctions.easeInOutSine(dt/t) : 1.0;
				pvars.correctionOffset.x = pvars.correctionOffset.aniDstX * d + pvars.correctionOffset.aniSrcX * (1.0-d);
				pvars.correctionOffset.y = pvars.correctionOffset.aniDstY * d + pvars.correctionOffset.aniSrcY * (1.0-d);
			}
		}
		$StaticPrivate.setCorrectionOffset = function(self, x, y) {
			var t = $StaticPrivate.msecs;
			pvars.correctionOffset.aniStartTime = t;
			pvars.correctionOffset.aniEndTime = t+300;
			pvars.correctionOffset.aniSrcX = pvars.correctionOffset.x;
			pvars.correctionOffset.aniSrcY = pvars.correctionOffset.y;
			pvars.correctionOffset.aniDstX = x;
			pvars.correctionOffset.aniDstY = y;
		}

		pvars.onscreenElements = [];
		pvars.selectedDataElementID = undefined;
		pvars.siblings = [];

		pvars.mouseOver = false;
		pvars.lastMouseRelease = 0;

		// for animation:
		pvars.animationStack = [];

		pvars.anim = {
			done : true,
			delta : 0.0,
			type : undefined,
			onComplete : undefined,
			startTime : 0,
			endTime : 0,
			srcOffsetX : 0.0,
			srcOffsetY : 0.0,
			dstOffsetX : 0.0,
			dstOffsetY : 0.0,
			srcZoom : 1.0,
			dstZoom : 1.0
		}

		$StaticPrivate.IsFunction = function(f) {
			return f && {}.toString.call(f) === '[object Function]';
		}


		$StaticPrivate.Animation = {
			LOOKAT : 1
		}

		$StaticPrivate.updateAnim = function(self) {
			if (pvars.anim.done) return;

			var a = pvars.anim;

			if (a.endTime - a.startTime <= 0.0) {
				a.delta = 1.0;
			}
			else {
				a.delta = ($StaticPrivate.msecs - a.startTime) / (a.endTime - a.startTime);
			}

			if (a.delta >= 1.0) {
				a.delta = 1.0;
				a.done = true;
				if ($StaticPrivate.IsFunction(a.onComplete)) a.onComplete();
				if (pvars.animationStack.length > 0) $StaticPrivate.initAnimation(self, pvars.animationStack.shift());
			}

			var animDelta = $StaticPrivate.easingFunctions.easeInOutSine(a.delta);

			switch(a.type) {
			case $StaticPrivate.Animation.LOOKAT:

				pvars.offsetX = a.srcOffsetX*(1.0-animDelta) + a.dstOffsetX*animDelta;
				pvars.offsetY = a.srcOffsetY*(1.0-animDelta) + a.dstOffsetY*animDelta;
				pvars.zoom = a.srcZoom*(1.0-animDelta) + a.dstZoom*animDelta;
				break;
			}

			self.requestRedraw();
		}

		$StaticPrivate.startAnimation = function(self, animation) {
			if (pvars.anim.done) {
				$StaticPrivate.initAnimation(self, animation);
			}
			else {
				pvars.animationStack.push(animation);
			}
		}

		$StaticPrivate.lookAt = function(self, eid, onComplete=undefined) {
			$StaticPrivate.startAnimation(self, {
				type : $StaticPrivate.Animation.LOOKAT,
				elemID : eid,
				onComplete : onComplete
			});
		}
		$StaticPrivate.lookAtFast = function(self, eid, onComplete=undefined) {
			$StaticPrivate.startAnimation(self, {
				type : $StaticPrivate.Animation.LOOKAT,
				elemID : eid,
				onComplete : onComplete,
				fast : true
			});
		}

		$StaticPrivate.initAnimation = function(self, animation) {
			switch (animation.type) {
			case $StaticPrivate.Animation.LOOKAT:
				var eid = animation.elemID;
				if (!pvars.anim.done) return;

				var a = pvars.anim;
				var e = undefined;
				if (eid >= 0 && eid < pvars.currentView._data.length) e = pvars.currentView._data[eid];


				a.type = $StaticPrivate.Animation.LOOKAT;
				a.done = false;
				a.delta = 0.0;
				a.onComplete = animation.onComplete; // TODO call something here ...
				a.startTime = $StaticPrivate.msecs;
				a.endTime = a.startTime + (animation.fast?1:300); // TODO: get animation time from settings instead of using 500
				a.srcZoom = pvars.zoom;
				a.dstZoom = e!=undefined ? Math.max(pvars.zoom, 2.5) : 1.0;
				a.srcOffsetX = pvars.offsetX;
				a.srcOffsetY = pvars.offsetY;
				a.dstOffsetX = 0.5;
				a.dstOffsetY = 0.5;
				if (e != undefined) {
					a.dstOffsetX = (-e.screenPosX*a.dstZoom/a.srcZoom)/pvars.canvas.width+0.5;
					a.dstOffsetY = (-e.screenPosY*a.dstZoom/a.srcZoom)/pvars.canvas.height+0.5;
				}

				break;
			}
		}

		$StaticPrivate.drawRoundedRect = function (ctx, x, y, w, h, r) {
			if (w < 2 * r) r = w / 2;
			if (h < 2 * r) r = h / 2;
			ctx.beginPath();
			ctx.moveTo(x+r, y);
			ctx.arcTo(x+w, y,   x+w, y+h, r);
			ctx.arcTo(x+w, y+h, x,   y+h, r);
			ctx.arcTo(x,   y+h, x,   y,   r);
			ctx.arcTo(x,   y,   x+w, y,   r);
			ctx.closePath();
		}

		$StaticPrivate.resetFilter = function(self) {
			for (var i=0; i<pvars.currentView._data.length; ++i) {
				pvars.currentView._data[i].filter = true;
			}
			self.requestRedraw();
		}
		$StaticPrivate.filterRing = function(self, ring) {
			var rng = pvars.currentView.fields.ring;
			for (var i=0; i<pvars.currentView._data.length; ++i) {
				var e = pvars.currentView._data[i];
				var d = pvars.data[e.ptr];
				if (e.filter && d[rng] != ring) {
					e.filter = false;
				}
			}
			for (var i=0; i<pvars.currentView._data.length; ++i) {
				var e = pvars.currentView._data[i];
				var d = pvars.data[e.ptr];
			}
			self.requestRedraw();
		}
		$StaticPrivate.filterSection = function(self, section) {
			var sec = pvars.currentView.fields.section;
			for (var i=0; i<pvars.currentView._data.length; ++i) {
				var e = pvars.currentView._data[i];
				var d = pvars.data[e.ptr];
				if (e.filter && d[sec] != section) {
					e.filter = false;
				}
			}
			self.requestRedraw();
		}
		$StaticPrivate.filterClass = function(self, classID) {
			var cls = pvars.currentView.fields['class'];
			for (var i=0; i<pvars.currentView._data.length; ++i) {
				var e = pvars.currentView._data[i];
				var d = pvars.data[e.ptr];
				if (e.filter && d[cls] != classID) {
					e.filter = false;
				}
			}
			self.requestRedraw();
		}
		$StaticPrivate.filterText = function(self, text) {
			text = text.toLowerCase();
			var txt = pvars.currentView.fields.text;
			var nam = pvars.currentView.fields.name;
			for (var i=0; i<pvars.currentView._data.length; ++i) {
				var e = pvars.currentView._data[i];
				var d = pvars.data[e.ptr];
				if (e.filter && (d[nam].toLowerCase().indexOf(text) < 0 && d[txt].toLowerCase().indexOf(text) < 0)) {
					e.filter = false;
				}
			}
			self.requestRedraw();
		}
		$StaticPrivate.filterDataElement = function(self, elementID = undefined, filterText = undefined) {
			$StaticPrivate.resetFilter(self);
			if (elementID != undefined) {
				var e = pvars.currentView._data[elementID];
				var d = pvars.data[e.ptr];
				$StaticPrivate.filterSection(self, d[pvars.currentView.fields.section]);
			}
			if (filterText) {
				$StaticPrivate.filterText(self, filterText);
			}
			pvars.currentView.updateDescriptionBox();
		}

		$StaticPrivate.borderCollision = function() {
				var scalex = pixelWidth>pixelHeight ? 1.0 : pixelWidth/pixelHeight;
				var scaley = pixelHeight>pixelWidth ? 1.0 : pixelHeight/pixelWidth;
				if (pvars.offsetX < -0.2*pvars.zoom/scalex) pvars.offsetX = 0-0.2*pvars.zoom/scalex;
				else if (pvars.offsetX > (scalex + 0.2*pvars.zoom)/scalex) pvars.offsetX = (scalex + 0.2*pvars.zoom)/scalex;
				if (pvars.offsetY < -0.2*pvars.zoom/scaley) pvars.offsetY = 0-0.2*pvars.zoom/scaley;
				else if (pvars.offsetY > (scaley + 0.2*pvars.zoom)/scaley) pvars.offsetY = (scaley + 0.2*pvars.zoom)/scaley;
		}

		$StaticPrivate.updateMouseRadarCoords = function() {
			pvars.mouseRadarX = (pvars.mouseScreenX - pvars.offsetX) * pvars.aspectRatio * Math.min(pvars.canvas.width, pvars.canvas.height);
			pvars.mouseRadarY = (pvars.mouseScreenY - pvars.offsetY) * Math.min(pvars.canvas.width, pvars.canvas.height);
		}

		$StaticPrivate.updateMouseOverInfo = function() {
			var heOld = pvars.hoverDataElementID;
			pvars.hoverDataElementID = undefined;
			var rcm = 0.5*Radar.DPCM;
			var minDistSqr = 99999999;
			if (pvars.currentView && pvars.currentView._data) {
				for (var i=0; i<pvars.currentView._data.length; ++i) {
					var e = pvars.currentView._data[i];
					if (e.visible==0.0) continue;
					var dx = e.screenPosX - pvars.mouseRadarX;
					var dy = e.screenPosY - pvars.mouseRadarY;
					var r = Math.max(e.screenRadius, rcm);
					var distSqr = dx*dx + dy*dy;
					// TODO: min radius should be 0.5 cm !!!
					if (distSqr < minDistSqr && dx*dx + dy*dy <= r*r) {
						minDistSqr = distSqr;
						pvars.hoverDataElementID = i;
					}
				}
			}
			if (pvars.hoverDataElementID != heOld) self.requestRedraw();
		}

		$StaticPrivate.forceMouseRelease = function(self) {
			var pvars = $Private[self];
			pvars.isDragging = false;
			pvars.mouseDown = false;
			pvars.dragPixelsMaxDist = 0.0;
			if($StaticPrivate.msecs - pvars.lastMouseRelease > 100) pvars.lastMouseRelease = $StaticPrivate.msecs;
		}

		can.addEventListener('mouseout', e => {
			// ...
		});
		can.addEventListener('mouseleave', e => {
			pvars.mouseOver = false;

			if (pvars.hoverDataElementID != undefined) {
				pvars.hoverDataElementID = undefined;
				self.requestRedraw();
			}
		});
		can.addEventListener('mousemove', e => {
			var ex = e.offsetX*pvars.rescale - pvars.correctionOffset.x;
			var ey = e.offsetY*pvars.rescale - pvars.correctionOffset.y;
			pvars.mouseScreenX = ex/pixelWidth;
			pvars.mouseScreenY = ey/pixelHeight;

			if (pvars.mouseDown) {
				// check distance from drag start position
				var dx = pvars.dragStartPixelsX-ex;
				var dy = pvars.dragStartPixelsY-ey;
				let dist;
				dist = Math.sqrt(dx*dx+dy*dy);
				if (dist > pvars.dragPixelsMaxDist) pvars.dragPixelsMaxDist = dist;
				if (!pvars.isDragging) {
					var dist_cm = dist/Radar.DPCM;
					if (dist_cm > 0.25) pvars.isDragging = true;
				}
			}
			if (pvars.isDragging) {
				self.requestRedraw();
				pvars.offsetX = pvars.dragOffsetX + (pvars.mouseScreenX-pvars.dragStartX);
				pvars.offsetY = pvars.dragOffsetY + (pvars.mouseScreenY-pvars.dragStartY);

				$StaticPrivate.borderCollision();

			}
			$StaticPrivate.updateMouseRadarCoords();
			$StaticPrivate.updateMouseOverInfo();
		});
		can.addEventListener('mousedown', e => {
			self.requestRedraw();
			if($StaticPrivate.msecs - pvars.lastMouseRelease < 100) return
			var ex = e.offsetX*pvars.rescale - pvars.correctionOffset.x;
			var ey = e.offsetY*pvars.rescale - pvars.correctionOffset.y;
			$StaticPrivate.updateMouseRadarCoords();
			pvars.dragStartX = ex/pixelWidth;
			pvars.dragStartY = ey/pixelHeight;
			pvars.dragStartPixelsX = ex;
			pvars.dragStartPixelsY = ey;
			pvars.dragOffsetX = pvars.offsetX;
			pvars.dragOffsetY = pvars.offsetY;
			pvars.mouseDown = true;
			pvars.dragPixelsMaxDist = 0.0;
		});
		window.addEventListener('mouseup', e => {
			if (pvars.mouseOver && pvars.isDragging == false) {
				$StaticPrivate.selectDataElement( self, pvars.hoverDataElementID );
				$StaticPrivate.filterDataElement( self, pvars.selectedDataElementID, pvars.searchBox.value );
			}
			$StaticPrivate.forceMouseRelease(self);
			self.requestRedraw();
		});
		can.addEventListener('click', e => {
			$StaticPrivate.forceMouseRelease(self);
		});
		can.addEventListener('mouseover', e => {
			pvars.mouseOver = true;
		});

		can.addEventListener('wheel', e => {
			self.requestRedraw();
			var oldZoom = pvars.zoom;
			pvars.zoom *= 1.0 + (e.deltaY * -0.002);
			if (pvars.zoom < 0.5) pvars.zoom = 0.5;
			if (pvars.zoom > 10.0) pvars.zoom = 10.0;

			var ox = pvars.offsetX;
			var oy = pvars.offsetY;
			var dx = pvars.mouseScreenX-pvars.offsetX;
			var dy = pvars.mouseScreenY-pvars.offsetY;
			pvars.offsetX = ox+dx-dx*(pvars.zoom/oldZoom);
			pvars.offsetY = oy+dy-dy*(pvars.zoom/oldZoom);

			$StaticPrivate.borderCollision();

			$StaticPrivate.updateMouseOverInfo();
			$StaticPrivate.updateMouseRadarCoords();

			e.preventDefault();
		});

		$StaticPrivate.createLink = function(title, onfocus, onblur, onclick) {
			let link;
			link = document.createElement('a');
			link.href = "javascript:void(0)";
			link.style.display = "none";
			link.title = title;
			link.__visible = false;
			link.onfocus = onfocus;
			link.onblur = onblur;
			link.onclick = onclick;
			return link;
		}
		pvars.hideLink = function(link) {
			if (link.__visible == false) return;
			link.__visible = false;
			can.removeChild(link);
		}
		pvars.showLink = function(link) {
			if (link.__visible == true) return;
			link.__visible = true;
			can.appendChild(link);
		}

		pvars.currentView = undefined;
		pvars.views = {};

		for (var i=0; i<pvars.settings.views.length; ++i) {
			var view = pvars.settings.views[i];
			if (!('id' in view)) continue;
			if (view.id in pvars.views) {
				console.warn('duplicate view id "'+view.id+'"');
			}
			else {
				pvars.views[view.id] = view;
				pvars.addViewLink(view.id);
				if (pvars.currentView == undefined) pvars.currentView = view;
			}
		}
		$StaticPrivate.updateLinks(self, pvars.currentView);

		pvars.parent.appendChild(pvars.canvas);
		pvars.ctx = pvars.canvas.getContext("2d");

		pvars.contentDiv = document.createElement('div');
		pvars.contentDiv.style.position = "absolute"
		pvars.contentDiv.style.left = "0px";
		pvars.contentDiv.style.top = "0px";
		pvars.contentDiv.style.width = "100%";
		pvars.contentDiv.style.zIndex = "2";
		pvars.contentDiv.innerHTML = '<span class="radar-title">'+$StaticPrivate.escapeHTML(pvars.settings.title)+'</span>';

		pvars.parent.appendChild(pvars.contentDiv);


		//------------------------------
		pvars.decJobs();
	}

	$StaticPrivate.onrender = function(self) {
		var pvars = $Private[self];
		if (pvars.destroyed || pvars.ctx==null) return;

		if (!pvars.ready) self.requestRedraw();

		// set mouse cursor:
		if (pvars.mouseDown == true) {
			pvars.canvas.style.cursor = 'grabbing';
		}
		else if (pvars.hoverDataElementID != undefined) {
			pvars.canvas.style.cursor = 'pointer';
		}
		else {
			pvars.canvas.style.cursor = 'grab';
		}

		if (!pvars.anim.done) $StaticPrivate.updateAnim(self);

		// update sideDiv animation:
		$StaticPrivate.updateSideDiv(self);

		// update viewSwitchAnimation:
		$StaticPrivate.updateViewSwitchAni(self);

		if($StaticPrivate.msecs-pvars.readyTime<1500 || $StaticPrivate.msecs<pvars.correctionOffset.aniEndTime) pvars.redraw = true;
		if (!pvars.redraw) return;
		pvars.redraw = false;


		var ctx = pvars.ctx;
		var width = pvars.canvas.width;
		var height = pvars.canvas.height;

		// nice round lines:
		ctx.lineJoin = "round";
		ctx.lineCap = "round";

		// update correctionOffset
		$StaticPrivate.updateCorrectionOffset(self);

		// clear:
		ctx.setTransform(1.0,0.0,0.0,1.0,0.0,0.0);
		ctx.clearRect(0, 0, width, height);

		var radius = Math.min(width, height)*0.4 * pvars.zoom;

		if (pvars.ready) {
			// push transformation matrix:
			ctx.save();

			delta = 1.0;
			if($StaticPrivate.msecs-pvars.readyTime<1500) {
				var delta = ($StaticPrivate.msecs-pvars.readyTime)/1500.0;

				delta = $StaticPrivate.getEasingFunc(pvars.settings.show_radar_easing)(delta);
			}
			ctx.setTransform(1.0*delta,0.0,0.0,1.0*delta,pvars.offsetX*width, pvars.offsetY*height);
			ctx.translate(pvars.correctionOffset.x, pvars.correctionOffset.y);
			ctx.rotate(-(1.0-delta)*0.5);

			ctx.fillStyle = pvars.settings.radar_background_color;
			ctx.strokeStyle = pvars.settings.radar_line_color;

			// draw radar background:
			ctx.beginPath();
			ctx.arc(0, 0, radius, 0, 2 * Math.PI);
			ctx.fill();
			// draw radar outline:
			ctx.lineWidth = (1.0*pvars.rescale);
			ctx.stroke();

			// if no views defined:
			if ( Object.entries( pvars.settings.views ).length === 0 ) {
				ctx.fillStyle = pvars.settings.radar_inner_text_color;
				ctx.font = "italic "+(32*pvars.rescale)+"px sans-serif";
				ctx.textAlign = "center";
				ctx.fillText("no views defined ...", 0, 16);
			}
			else if ( pvars.currentView == undefined ) {
				ctx.fillStyle = pvars.settings.radar_inner_text_color;
				ctx.font = "italic "+(32*pvars.rescale)+"px sans-serif";
				ctx.textAlign = "center";
				ctx.fillText("no view selected ...", 0, 16);
			}
			else {
				// draw rings:
				ctx.setLineDash([3.5*pvars.zoom, 3*pvars.zoom]);
				for (var i=0; i<pvars.currentView._radii.length-1; ++i) {
					ctx.beginPath();
					ctx.arc(0, 0, radius*pvars.currentView._radii[i], 0, 2 * Math.PI);
					ctx.stroke();
				}
				ctx.setLineDash([]);

				// draw segments:
				var secs = pvars.currentView.section_order.length;
				ctx.beginPath();
				for (var i=0; i<secs; ++i) {
					ctx.moveTo(0, 0);
					ctx.lineTo(Math.cos(2*Math.PI * i / secs + pvars.currentView.rotation) * radius, Math.sin(2*Math.PI * i / secs + pvars.currentView.rotation) * radius);
				}
				ctx.stroke();

				ctx.textAlign = "center";

				// draw data elements:
				var elemResize = pvars.zoom<1.0 ? 1.0 : -0.05555*pvars.zoom+1.05555;
				var vd = pvars.viewSwitchAnimation.delta;
				var eo = undefined;
				var symbolFont = (15*elemResize*pvars.zoom*pvars.rescale)+'px radaricons';
				var symbolFontSelected = (18*elemResize*pvars.zoom*pvars.rescale)+'px radaricons';
				var yOff = 4.0*elemResize*pvars.zoom*pvars.rescale/radius;
				var smbl, ex, ey, offset;
				ctx.strokeStyle = '#8F8';
				ctx.lineWidth = 2;
				for(var i=0; i<pvars.currentView._data.length; ++i) {
					var e = pvars.currentView._data[i];
					if (e.visible==0.0 && (vd==1.0 || (!pvars.viewSwitchAnimation.srcView || pvars.viewSwitchAnimation.srcView._data[i].visible==0.0))) continue;
					ctx.globalAlpha = 1.0;
					if (vd>=1.0) {
						ctx.fillStyle = e.filter ? e.color : pvars.settings.radar_unselected_items_color;
					}
					else {
						eo = pvars.viewSwitchAnimation.srcView._data[i];
						if (e.visible) { ctx.fillStyle = e.filter ? e.color : pvars.settings.radar_unselected_items_color; }
						else { ctx.fillStyle = eo.filter ? eo.color : pvars.settings.radar_unselected_items_color; }

						if (e.visible) {
							if (eo.visible) { ctx.globalAlpha = 1.0; }
							else { ctx.globalAlpha = vd; }
						}
						else if (eo.visible) {
							ctx.globalAlpha = (1.0-vd);
						}
					}

					smbl = $StaticPrivate.getIconString(pvars.currentView.class_symbols[pvars.data[e.ptr][pvars.currentView.fields['class']]]);
					offset = (e.ptr == pvars.selectedDataElementID ? $StaticPrivate.offsetCorrectionSelected : $StaticPrivate.offsetCorrection)[smbl] * pvars.zoom*pvars.rescale;
					if (vd >= 1.0) {
						ex = e.x*radius;
						ey = (e.y+yOff)*radius;
					}
					else {
						ex = (e.x*vd+eo.x*(1.0-vd))*radius;
						ey = ((e.y+yOff)*vd+(eo.y+yOff)*(1.0-vd))*radius;
					}
					ctx.font = e.ptr == pvars.selectedDataElementID ? symbolFontSelected : symbolFont;
					ctx.fillText(smbl, ex, ey+offset);

					// update data elements screen coordinates:
					e.screenPosX = e.x*radius;
					e.screenPosY = e.y*radius;
					e.screenRadius = 5*elemResize*pvars.zoom*pvars.rescale;

					if (e.focus) {
						ctx.setLineDash([0.45*e.screenRadius, 0.45*e.screenRadius]);
						ctx.strokeStyle = pvars.settings.radar_focus_item_color;
						ctx.beginPath();
						ctx.arc(ex, ey-e.screenRadius*0.8, 2*e.screenRadius, 0, 2 * Math.PI);
						ctx.stroke();
						ctx.setLineDash([]);
					}
				}
				ctx.globalAlpha = 1.0;

				// outline selected element (and siblings):
				for (var i=0; i<pvars.siblings.length; ++i) {
						var e = pvars.currentView._data[pvars.siblings[i]];
						if (e.visible == 0.0) continue;

						smbl = $StaticPrivate.getIconString(pvars.currentView.class_symbols[pvars.data[e.ptr][pvars.currentView.fields['class']]]);
						offset = (e.ptr == pvars.selectedDataElementID ? $StaticPrivate.offsetCorrectionSelected : $StaticPrivate.offsetCorrection)[smbl] * pvars.zoom*pvars.rescale;;
						if (vd >= 1.0) { ex = e.x*radius; ey = (e.y+yOff)*radius; }
						else { ex = (e.x*vd+eo.x*(1.0-vd))*radius; ey = ((e.y+yOff)*vd+eo.y*(1.0-vd))*radius; }
						ctx.font = (pvars.siblings[i] == pvars.selectedDataElementID ? symbolFontSelected : symbolFont);
						ctx.lineWidth = 6;
						ctx.strokeStyle = e.filter ? e.color : pvars.settings.radar_unselected_items_color;
						ctx.strokeText(smbl, ex, ey+offset);
						ctx.lineWidth = 2;
						ctx.strokeStyle = pvars.settings.radar_items_text_color;
						ctx.strokeText(smbl, ex, ey+offset);
				}


				// draw element numbers
				ctx.lineWidth = 1*pvars.zoom*pvars.rescale;
				if (pvars.zoom > 0.999) {
					ctx.fillStyle = pvars.settings.radar_items_text_color;
					var font1 = 'bold '+(7*elemResize*pvars.zoom*pvars.rescale)+'px sans-serif';
					var font2 = 'bold '+(5*elemResize*pvars.zoom*pvars.rescale)+'px sans-serif';
					var font1_selected = 'bold '+(9*elemResize*pvars.zoom*pvars.rescale)+'px sans-serif';
					var font2_selected = 'bold '+(7*elemResize*pvars.zoom*pvars.rescale)+'px sans-serif';
					var txtX, txtY;
					for(var i=0; i<pvars.currentView._data.length; ++i) {
						var e = pvars.currentView._data[i];
						var sel = e.ptr == pvars.selectedDataElementID;
						if (e.visible==0.0) continue;
						ctx.font = e.ptr+1 < 100
							? (sel ? font1_selected : font1)
							: (sel ? font2_selected : font2);
						ctx.strokeStyle = e.filter ? e.color : pvars.settings.radar_unselected_items_color;
						if (vd >= 1.0) {
							txtX = e.x*radius;
							txtY = e.y*radius+(e.ptr+1<100?(sel?3.21:2.5):(sel?2.45:1.75))*elemResize*pvars.zoom*pvars.rescale;
							ctx.strokeText(e.ptr+1, txtX, txtY);
							ctx.fillText(e.ptr+1, txtX, txtY);
						}
						else {
							var eo = pvars.viewSwitchAnimation.srcView._data[i];

							txtX = (e.x*vd+eo.x*(1.0-vd))*radius;
							txtY = (e.y*vd+eo.y*(1.0-vd))*radius+(e.ptr+1<100?(sel?3.21:2.5):(sel?2.45:1.75))*elemResize*pvars.zoom*pvars.rescale;
							ctx.strokeText(e.ptr+1, txtX, txtY);
							ctx.fillText(e.ptr+1, txtX, txtY);
						}
					}
				}
				ctx.globalAlpha = 1.0;

				var labelZoom = pvars.zoom<1.0?pvars.zoom:1.0;
				ctx.fillStyle = pvars.settings.radar_outer_text_color;
				ctx.font = (12*labelZoom*pvars.rescale)+'px sans-serif';
				var fontHeight = (12*labelZoom*pvars.rescale);
				ctx.textAlign = "center";

				// draw section labels:
				var sectionOutlineLength = 2*Math.PI*radius / secs;
				for (var i=0; i<secs; ++i) {
					ctx.save();
					var onTop = ((((2*Math.PI*(i+0.5))/secs + pvars.currentView.rotation)%(2*Math.PI))+(2*Math.PI))%(2*Math.PI)<Math.PI?true:false;
					ctx.rotate(2*Math.PI * (i+0.5) / secs + pvars.currentView.rotation - (onTop ? Math.PI*0.5 : Math.PI*1.5) );
					var txt =
						pvars.currentView.section_order[i] in pvars.currentView.section_names
						? pvars.currentView.section_names[ pvars.currentView.section_order[i] ]
						: pvars.currentView.section_order[i];
					if (ctx.measureText(txt).width < sectionOutlineLength) {
						ctx.fillCircleText(txt, 0, 0, radius+(onTop?5+fontHeight-2:5), onTop ? Math.PI*0.5 : Math.PI*1.5);
					}
					else {
						var words = txt.split(' ');
						var lines = [];
						var word = '';
						let s;
						s = '';
						for (var k=0; k<words.length; ++k) {
							word = words[k];
							if (s=='' || ctx.measureText(s + ' ' + word).width < sectionOutlineLength) {
								if (s!='') s += ' ';
								s += ' ' + word;
							}
							else {
								lines.push(s);
								s = word;
							}
						}
						if (s!='') lines.push(s);
						for (var k=0; k<lines.length; ++k) {
							ctx.fillCircleText(lines[k], 0, 0, radius+(onTop?5+fontHeight-2:5) + (fontHeight+2)*(onTop?k:lines.length-k-1), onTop ? Math.PI*0.5 : Math.PI*1.5);
						}
					}

					ctx.restore();
				}

				// draw ring labels:
				ctx.fillStyle = pvars.settings.radar_inner_text_color;
				ctx.strokeStyle = pvars.settings.radar_background_color;
				ctx.lineWidth = 2.0;
				ctx.font = (10*labelZoom*pvars.rescale)+'px sans-serif';
				ctx.textAlign = "center";
				ctx.save();
				ctx.rotate(pvars.currentView._ring_label_rotation);
				for (var i=0; i<pvars.currentView.ring_order.length; ++i) {
					var r0 = i==0?0:pvars.currentView._radii[i-1]*radius;
					var r1 = pvars.currentView._radii[i]*radius;
					var dr = r1-r0;
					var txt = pvars.currentView.ring_names[ pvars.currentView.ring_order[i] ] || "";
					if (ctx.measureText(txt).width > dr) {
						while (true) {
							txt = txt.substring(0, txt.length - 1) || '';
							if (txt.length == 0 || ctx.measureText(txt+'...').width < dr) {
								txt += '...';
								break;
							}
						}
					}
					var r = r0+(dr)/2;
					ctx.strokeText(txt, r, -2);
					ctx.fillText(txt, r, -2);
				}
				ctx.restore();
			}

				// draw tooltip
				if (pvars.hoverDataElementID != undefined) {
					var e = pvars.currentView._data[pvars.hoverDataElementID];
					var rs = pvars.rescale;
					var ts = (16*rs);
					ctx.font = 'bold '+ts+'px sans-serif';
					var txt = pvars.data[e.ptr][pvars.currentView.fields.name];
					var tw = ctx.measureText(txt).width;
					var th = ts;
					var tx = e.x*radius;
					var ty = e.y*radius - e.screenRadius-10*rs;

					if (pvars.settings.radar_show_tooltip_shadow) {
						ctx.shadowBlur = pvars.settings.radar_tooltip_shadow_blur;
						ctx.shadowColor = pvars.settings.radar_tooltip_shadow_color;
						ctx.shadowOffsetX = pvars.settings.radar_tooltip_shadow_offset.x;
						ctx.shadowOffsetY = pvars.settings.radar_tooltip_shadow_offset.y;
					}
					else {
						ctx.shadowBlur = 0;
						ctx.shadowColor = "#0000";
						ctx.shadowOffsetX = 0;
						ctx.shadowOffsetY = 0;
					}

					ctx.fillStyle = pvars.settings.radar_tooltip_background_color;
					ctx.beginPath();
					$StaticPrivate.drawRoundedRect(ctx, tx-(tw+8*rs)/2, ty-(th+8*rs), tw+8*rs, th+8*rs, 8*rs)
					ctx.fill();

					ctx.shadowBlur = 0;
					ctx.shadowColor = "#0000";
					ctx.shadowOffsetX = 0;
					ctx.shadowOffsetY = 0;

					ctx.fillStyle = pvars.settings.radar_tooltip_text_color;
					ctx.fillText(txt, tx, ty-6*rs);
				}

			// pop transformation matrix:
			ctx.restore();
		}

		// loading animation:
		if (!pvars.ready || ($StaticPrivate.msecs-pvars.readyTime<500)) {
			if($StaticPrivate.msecs-pvars.readyTime<500) {
				var delta = ($StaticPrivate.msecs-pvars.readyTime)/500.0;
				ctx.lineWidth = 8.0*(1.0-delta);
			}
			else {
				ctx.lineWidth = 8;
			}
			ctx.strokeStyle = pvars.settings.highlight_color;
			var a = ( ($StaticPrivate.msecs/400.0)%2.0)*Math.PI;
			ctx.setTransform(1.0,0.0,0.0,1.0,width/2,height/2);
			ctx.beginPath();
			ctx.arc(
				0.0, 0.0, 50.0,
				a,
				a + (0.25+(Math.sin($StaticPrivate.msecs/300.0)*0.4+0.5))*Math.PI
			);
			ctx.stroke();
			return;
		}

	}

	$StaticPrivate.onload = function(){
		var r = document.getElementsByClassName("radar");
		for(var i=0; i<r.length; ++i) {
			var e = r[i];

			if ('src' in e.dataset) {
				new Radar(e.dataset.src, e);
			}
			else {
				new Radar('{}', e);
			}

			r[i].innerHTML = '';
		}
	}

	$StaticPrivate.updateLinks = function(self, view, updateAll = true) {
		//TODO: hide stuff from other views ...
		let pvars;
		pvars = $Private[self];

		if (updateAll) {
			for (const k in pvars.views) {
				var v = pvars.views[k];
				if (!v._data) continue;
				for (var i=0; i<v._data.length; ++i){
					pvars.hideLink(v._data[i].link);
				}
			}
		}
		else {
			for (var i=0; i<view._data.length; ++i){
				pvars.hideLink(view._data[i].link);
			}
		}

		if (view && view.hasOwnProperty('_data')) {
			for (var i=0; i<view._data.length; ++i){
				var e = view._data[i];
				if (e.visible>0.0 && e.filter) pvars.showLink(e.link);
			}
		}
	}

	$StaticPrivate.renderAll = function() {
		$StaticPrivate.msecs = (new Date()).getTime() - $StaticPrivate.startTime;

		for (var i=0; i<$StaticPrivate.radarList.length; ++i) {
			$StaticPrivate.onrender( $StaticPrivate.radarList[i] );
		}

		window.requestAnimationFrame($StaticPrivate.renderAll);
	}


	function easeLinerar(x) {
		return x;
	}

	function easeInSine(x) {
		return 1 - Math.cos((x * Math.PI) / 2);
	}
	function easeOutSine(x) {
		return Math.sin((x * Math.PI) / 2);
	}
	function easeInOutSine(x) {
		return -(Math.cos(Math.PI * x) - 1) / 2;
	}

	function easeInQuad(x) {
		return x * x;
	}
	function easeOutQuad(x) {
		return 1 - (1 - x) * (1 - x);
	}
	function easeInOutQuad(x) {
		return x < 0.5 ? 2 * x * x : 1 - Math.pow(-2 * x + 2, 2) / 2;
	}

	function easeInCubic(x) {
		return x * x * x;
	}
	function easeOutCubic(x) {
		return 1 - Math.pow(1 - x, 3);
	}
	function easeInOutCubic(x) {
		return x < 0.5 ? 4 * x * x * x : 1 - Math.pow(-2 * x + 2, 3) / 2;
	}

	function easeInQuart(x) {
		return x * x * x * x;
	}
	function easeOutQuart(x) {
		return 1 - Math.pow(1 - x, 4);
	}
	function easeInOutQuart(x) {
		return x < 0.5 ? 8 * x * x * x * x : 1 - Math.pow(-2 * x + 2, 4) / 2;
	}

	function easeInQuint(x) {
		return x * x * x * x * x;
	}
	function easeOutQuint(x) {
		return 1 - Math.pow(1 - x, 5);
	}
	function easeInOutQuint(x) {
		return x < 0.5 ? 16 * x * x * x * x * x : 1 - Math.pow(-2 * x + 2, 5) / 2;
	}

	function easeInExpo(x) {
		return x === 0 ? 0 : Math.pow(2, 10 * x - 10);
	}
	function easeOutExpo(x) {
		return x === 1 ? 1 : 1 - Math.pow(2, -10 * x);
	}
	function easeInOutExpo(x) {
		return x === 0
			? 0
			: x === 1
			? 1
			: x < 0.5 ? Math.pow(2, 20 * x - 10) / 2
			: (2 - Math.pow(2, -20 * x + 10)) / 2;
	}

	function easeInCirc(x) {
		return 1 - Math.sqrt(1 - Math.pow(x, 2));
	}
	function easeOutCirc(x) {
		return Math.sqrt(1 - Math.pow(x - 1, 2));
	}
	function easeInOutCirc(x) {
		return x < 0.5
			? (1 - Math.sqrt(1 - Math.pow(2 * x, 2))) / 2
			: (Math.sqrt(1 - Math.pow(-2 * x + 2, 2)) + 1) / 2;
	}

	function easeInBack(x) {
		const c1 = 1.70158;
		const c3 = c1 + 1;
		return c3 * x * x * x - c1 * x * x;
	}
	function easeOutBack(x) {
		const c1 = 1.70158;
		const c3 = c1 + 1;
		return 1 + c3 * Math.pow(x - 1, 3) + c1 * Math.pow(x - 1, 2);
	}
	function easeInOutBack(x) {
		const c1 = 1.70158;
		const c2 = c1 * 1.525;
		return x < 0.5
			? (Math.pow(2 * x, 2) * ((c2 + 1) * 2 * x - c2)) / 2
			: (Math.pow(2 * x - 2, 2) * ((c2 + 1) * (x * 2 - 2) + c2) + 2) / 2;
	}

	function easeInElastic(x) {
		const c4 = (2 * Math.PI) / 3;
		return x === 0
			? 0
			: x === 1
			? 1
			: -Math.pow(2, 10 * x - 10) * Math.sin((x * 10 - 10.75) * c4);
	}
	function easeOutElastic(x) {
		const c4 = (2 * Math.PI) / 3;
		return x === 0
			? 0
			: x === 1
			? 1
			: Math.pow(2, -10 * x) * Math.sin((x * 10 - 0.75) * c4) + 1;
	}
	function easeInOutElastic(x) {
		const c5 = (2 * Math.PI) / 4.5;
		return x === 0
			? 0
			: x === 1
			? 1
			: x < 0.5
			? -(Math.pow(2, 20 * x - 10) * Math.sin((20 * x - 11.125) * c5)) / 2
			: (Math.pow(2, -20 * x + 10) * Math.sin((20 * x - 11.125) * c5)) / 2 + 1;
	}

	function easeInBounce(x) {
		return 1 - easeOutBounce(1 - x);
	}
	function easeOutBounce(x) {
		const n1 = 7.5625;
		const d1 = 2.75;
		if (x < 1 / d1) {
			return n1 * x * x;
		} else if (x < 2 / d1) {
			return n1 * (x -= 1.5 / d1) * x + 0.75;
		} else if (x < 2.5 / d1) {
			return n1 * (x -= 2.25 / d1) * x + 0.9375;
		} else {
			return n1 * (x -= 2.625 / d1) * x + 0.984375;
		}
	}
	function easeInOutBounce(x) {
		return x < 0.5
			? (1 - easeOutBounce(1 - 2 * x)) / 2
			: (1 + easeOutBounce(2 * x - 1)) / 2;
	}

	$StaticPrivate.easingFunctions = {}
	$StaticPrivate.easingFunctions.easeLinerar = easeLinerar;
	$StaticPrivate.easingFunctions.easeInSine = easeInSine;
	$StaticPrivate.easingFunctions.easeOutSine = easeOutSine;
	$StaticPrivate.easingFunctions.easeInOutSine = easeInOutSine;
	$StaticPrivate.easingFunctions.easeInQuad = easeInQuad;
	$StaticPrivate.easingFunctions.easeOutQuad = easeOutQuad;
	$StaticPrivate.easingFunctions.easeInOutQuad = easeInOutQuad;
	$StaticPrivate.easingFunctions.easeInCubic = easeInCubic;
	$StaticPrivate.easingFunctions.easeOutCubic = easeOutCubic;
	$StaticPrivate.easingFunctions.easeInOutCubic = easeInOutCubic;
	$StaticPrivate.easingFunctions.easeInQuart = easeInQuart;
	$StaticPrivate.easingFunctions.easeOutQuart = easeOutQuart;
	$StaticPrivate.easingFunctions.easeInOutQuart = easeInOutQuart;
	$StaticPrivate.easingFunctions.easeInQuint = easeInQuint;
	$StaticPrivate.easingFunctions.easeOutQuint = easeOutQuint;
	$StaticPrivate.easingFunctions.easeInOutQuint = easeInOutQuint;
	$StaticPrivate.easingFunctions.easeInExpo = easeInExpo;
	$StaticPrivate.easingFunctions.easeOutExpo = easeOutExpo;
	$StaticPrivate.easingFunctions.easeInOutExpo = easeInOutExpo;
	$StaticPrivate.easingFunctions.easeInCirc = easeInCirc;
	$StaticPrivate.easingFunctions.easeOutCirc = easeOutCirc;
	$StaticPrivate.easingFunctions.easeInOutCirc = easeInOutCirc;
	$StaticPrivate.easingFunctions.easeInBack = easeInBack;
	$StaticPrivate.easingFunctions.easeOutBack = easeOutBack;
	$StaticPrivate.easingFunctions.easeInOutBack = easeInOutBack;
	$StaticPrivate.easingFunctions.easeInElastic = easeInElastic;
	$StaticPrivate.easingFunctions.easeOutElastic = easeOutElastic;
	$StaticPrivate.easingFunctions.easeInOutElastic = easeInOutElastic;
	$StaticPrivate.easingFunctions.easeInBounce = easeInBounce;
	$StaticPrivate.easingFunctions.easeOutBounce = easeOutBounce;
	$StaticPrivate.easingFunctions.easeInOutBounce = easeInOutBounce;

	$StaticPrivate.getEasingFunc = function(name) {
		if (name in $StaticPrivate.easingFunctions) return $StaticPrivate.easingFunctions[name];
		return $StaticPrivate.easingFunctions.easeLinerar;
	}



	$StaticPrivate.offsetCorrection = {
		'A' : 0.1, // SYMBOL_CIRCLE
		'B' : 0.0, // SYMBOL_TRIANGLE
		'C' : 0.1, // SYMBOL_SQUARE
		'D' : 0.05, // SYMBOL_DIAMOND
		'E' : -0.3, // SYMBOL_PENTAGON
		'F' : 0.1, // SYMBOL_HEXAGON
		'G' : 0.0, // SYMBOL_TREFOIL
		'H' : 0.1, // SYMBOL_QUATREFOIL
		'I' : 0.0, // SYMBOL_STAR
		'J' : 0.4, // SYMBOL_HEART
		'K' : -0.3, // SYMBOL_CURVED_TRIANGLE
		'L' : 0.8, // SYMBOL_TRIANGLE_2
		'M' : 0.5, // SYMBOL_PENTAGON_2
		'N' : 0.1, // SYMBOL_HEXAGON_2
		'O' : 0.8, // SYMBOL_CURVED_TRIANGLE_2
		'P' : 0.1  // SYMBOL_OCTAGON
	};

	$StaticPrivate.offsetCorrectionSelected = {
		'A' : 0.75, // SYMBOL_CIRCLE
		'B' : 0.0, // SYMBOL_TRIANGLE
		'C' : 0.7, // SYMBOL_SQUARE
		'D' : 0.5, // SYMBOL_DIAMOND
		'E' : 0.3, // SYMBOL_PENTAGON
		'F' : 0.7, // SYMBOL_HEXAGON
		'G' : 0.0, // SYMBOL_TREFOIL
		'H' : 0.7, // SYMBOL_QUATREFOIL
		'I' : 0.0, // SYMBOL_STAR
		'J' : 1.0, // SYMBOL_HEART
		'K' : 0.0, // SYMBOL_CURVED_TRIANGLE
		'L' : 1.8, // SYMBOL_TRIANGLE_2
		'M' : 1.0, // SYMBOL_PENTAGON_2
		'N' : 0.7, // SYMBOL_HEXAGON_2
		'O' : 1.5, // SYMBOL_CURVED_TRIANGLE_2
		'P' : 0.7  // SYMBOL_OCTAGON
	};

	$StaticPrivate.defaultSettings = {
		"width":"800px",
		"height":"600px",
		"maxres":2048,
		"title":"Data Radar",
		//"start_animation":"ZOOM_IN",
		//"info_text":"Lorem ipsum dolor sit amet ...",
		//"placement":"RANDOM",
		//"show_empty_sections":true,
		//"show_empty_rings":true,
		"show_legend_box":true,
		"show_views_box":true,
		//"show_labels":false,
		//"show_print_button":true,
		//"show_info_button":true,
		//"show_fullscreen_button":true,
		//"show_section_names":true,
		//"show_ring_names":true,
		//"show_search_bar":true,
		"views":[],

		"show_radar_easing":"easeOutQuart",
		//"show_radar_easing":"easeOutCirc",

		"radar_items_text_color" : "#FFF", // text color for item numbers

		"highlight_color":"#004488", // radar title and some other stuff ...
		"link_color":"#4488AA", // links ...
		"background_color":"#FFFFFF00", // background document
		"searchbox_border_color":"#999999", // border color of search box
		"searchbox_background_color":"#EEEEEE", // background color of search box
		"searchbox_text_color":"#333333", // text color of search box
		"searchbox_icon_color":"#004488", // icon color of search box
		"viewsbox_border_color":"#999999", // border color of views box
		"viewsbox_background_color":"#EEEEEE", // background color of views box
		"viewsbox_background_color_hover":"#DDDDDD", // background color of views box (when hovering)
		"viewsbox_text_color":"#333333", // text color of views box
		"legend_border_color":"#CCCCCC", // border color of legend box
		"legend_background_color":"#FFFFFF", // background color of legend box
		"legend_text_color":"#333333", // text color of legend box
		"sidebox_background_color":"#FFFFFFFF", // background color of side box
		"sidebox_background_color_hover":"#EEEEEE", // background color of side box (when hovering)
		"description_text_color":"#777777", // color of description text
		"sidebox_h_1_color":"#000000", // text color of side box heading 1
		"sidebox_h_2_color":"#000000", // text color of side box heading 2

		"radar_line_color":"#333333", // radar lines
		"radar_background_color":"#F6F6F6", // radar background color
		"radar_inner_text_color":"#333333",  // inner text (ring labels, error messages)
		"radar_outer_text_color":"#333333", // outer text (section labels)
		"radar_unselected_items_color":"#AAAAAA", // fill color of not highlighted elements
		"radar_focus_item_color" : "#00CC00",
		"radar_tooltip_background_color":"#333333DD",
		"radar_tooltip_text_color":"#FFFFFF",
		"radar_show_tooltip_shadow":false,
		"radar_tooltip_shadow_color":"#00000088",
		"radar_tooltip_shadow_blur":25,
		"radar_tooltip_shadow_offset":{
			"x":0,
			"y":5
		}

	}

	$StaticPrivate.initializedRadarCSS = false;
	$StaticPrivate.initRadarCSS = function(self) {
		var pvars = $Private[self];

		if ($StaticPrivate.initializedRadarCSS) return;
		$StaticPrivate.initializedRadarCSS = true;
		var rcss = `
@font-face {
	font-family: 'radaricons';
	src: url(data:application/x-font-woff;charset=utf-8;base64,d09GRk9UVE8AACg`+
`8AAoAAAAAPRAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABDRkYgAAADbAAAJFAAADdRahESpkZGVE0A`+
`ACe8AAAAHAAAAByNzNXhT1MvMgAAAVQAAABMAAAAYFlXZB1jbWFwAAAC4AAAAHYAAAFKRbcYOmhlY`+
`WQAAAD0AAAANgAAADYa5tNlaGhlYQAAASwAAAAgAAAAJAdyAxlobXR4AAAn2AAAAGQAAACGRbkFBm`+
`1heHAAAAFMAAAABgAAAAYAMVAAbmFtZQAAAaAAAAE/AAACOgOoQxZwb3N0AAADWAAAABMAAAAg/4Y`+
`AMgABAAAAAQAAm9grw18PPPUACwPoAAAAANvu/+UAAAAA2/CQQ//z/ysD9QMtAAAACAACAAAAAAAA`+
`eJxjYGRgYNb9r80Qxfzi/+f/n5m/MgBFUIAQALNzB5AAAFAAADEAAHicY2BhTmKcwMDKwMDUxbSHg`+
`YGhB0IzPmAwZGQCijKwMjPAACMDEghIc01haGBwZKhiVvhvwRDFrMtwFaaGqYPpGIMCEDICAEaVDC`+
`d4nIWPv0/CQBTHvwcFw2IMu8ktGkhKc23CgiNJB2McBNkcCL20TUqPXMvA6ubs5upo4p+nq9+W08W`+
`BNnfv8969H98H4ByfEDh+17h3LDDAi+MOzvDuuIsrfDv2MBCh4x4uxJPjPuPPzBTegN5dW9WwwBB7`+
`xx3OfXXcxS0+HHsYikvHPUhx47jPeIk5DHY4wCJHigw1JEbYYEwbQbXHJy/5ZrDFGhW9BTMy1nwhg`+
`ab12x9zszvYPM1qOdqMZaQi5ctlZrbrSi42mX1LtPV95j2i5KCcp2B5gpChss7rQifEB8ZSrlVwmK`+
`Wr032xJsQU0NQ11jJDtxIDCpSY8fzveoxPMSFHvH8XQmzKOjY21TIKlJzJv+nk6SSMJo32UzpX7eo`+
`V3xtdkn0VM4LWNpqw0rbKTSmVCgOllDzR8Af1aFnCAHicY2BgYGaAYBkGRgYQcAHyGMF8FgYNIM0G`+
`pBkZmBjCGKr+/wfyHRkS////f+D/Nqh6IGBkY0BwyAWMTMwsrGzsHJxc3Dy8fPwCgkLCIqJiCHlxC`+
`UkpaRlZOXkFRSVlFVU1dQ1NLW0dXT19A0ptpgoAAEosDkQAAHicY2BmAIP/zQxGDFgAAChEAbgAeJ`+
`ylOwlYFEezvbvs7oiIEVzBI7toPNAQD1A8omaiomi8Yjyj0aAQb1RQwHsErzio4K0gqBAVjRdINCo`+
`6aKKi0cRIovGIB+t9xGiwFwfoV9Wziyb53/f933tCiprururq6uo6eiY64uZGdDqd+4DIqWOnTogI`+
`b0F0eqIj7zqqEkdtnaOO3vG2weHrNlsd92pJyXFjHfJl1TqEvFWHPKxWhwTWaW3zIgYk8CctSLvOk`+
`yZPjxo7esxUP/9Rjf0Cmwc2D/DrP2bSxLBov09GjYk68U14RFRAQEDFXK8nhX/1yDukPmlAGpJGwK`+
`0xaULeJQHkPdKUNCPNgXsgCSItSSsSTFqTTqQz6UJCSFfSjYSS7qQH+Yj0JL1Ib9KH9CUfk37kE9K`+
`fDCADySAymAwhn5KhpAWKWZ8k6nrrftJvMEQafnfrZGxkSjOdMfcXKgm/Vprlbqrc2GNOFWOVHVVr`+
`V31YLc2rh7d/9fctB32/q3m+1i5re9syvwF1j9TrXb9Fg5iGL/xbNT7+3p1mQc1XtggJbBQU3nJnq`+
`9BWP5a+Lec5QvJ0eXlUyTPk1XDUd2SV1jfllcZaHCFUKQ0xe75Um7zy1THSgh195WtghlymUMXCiG`+
`ORyFgwAFIQpzBmrgDBro7XbdoQPthRAQpcHfho9XzJ9E+b/hYLMzUct5UZUiZnGujTksYWdV9pgpG`+
`xZ9MvM3an2VLGHM8uMFZkGSIDnFoHYEbTDYz02LyVsW2+4YyEv9gnC44fTYxEGLsw8tEvTxkZYlrJ`+
`yLDbD2wyI3VmNQQmv38I2G81gXHXpYzUyg8R1H0OnOjm4JeMtN68n7ETF39lpO7StoDJG2Geson1O`+
`TQwoJqE2GTAbmZ+tGjAuImDBFgBS22czwy2X7vq/sA/iwyM3PIjJX0tjB02gAZOXQOtnH6fwKMbsb`+
`GyK+6SmbFDv4MOWpdD/2mdxEhIXdHK9A2KRVhBl1JQVOvvCSNt3hHxUbJhF1CRLpeQVw3sfwCkh/0`+
`l0KHjZslGSzQjvhLMcVMEOg8RMT8ia3A8Yy8UaLknaUAorVla0/j6mXePl+GHk453ssKRDj6S+Nzs`+
`z9h929eM2M6GMPYXAVuw/XIQFOvXJABhy0tA+sFDoMRBvpsGAKUDKYFThiYQsSkgELHddEI+605NA`+
`BwmcGvwDkJrqLuXMEPmJmJwyCXxoMjyNTcYIR53AVu7ADDTF4CtC4H13UmYAfAubhi1DmbsoX8mbN`+
`T5YYJ6T5Vhjb+LJJqxJ5Nhut8+R4VcFhE+B30Q/VvQ+nwTYG4imSNQS+k7xtRBR0YfB1NqYzouH9m`+
`TegxaHe8Y+Yh1GgWpgnxIFeRDGqFekTtpCHxwtgyYmcrGmA1RK6ckwppgBessU+VpC2IlQc1XOxkj`+
`pkWPlEcI6jOzPCJj5L5pAsXWNDACC2r9FaoDMPkNyIhRImkeDWg+7WTctzljv3xAoEB/IHp/xGbgC`+
`q3TV8cmx+B8m1/1LZMce70lKjtEi1pjWoDqJqu+stqYVlGr0MbyVfnynqMFR3tdGn1NhkdahVbhf3`+
`3fpW6qZbPgLa7JSt4p7xRoYxhfRW0MP0CpNrYuHZs0bvVkmEPt/KqHLvNVDwPT/dVEKUmBHZptk0C`+
`tG3Rgn2uPwTJeRkmMzU0QEQD2cjJ2HAVsw2DU/2w/HA5EjIz5EFvxEEizcOQUwCotdRKSCEY0jiT+`+
`JpA4YUQWQUIAlZYRjVADlZJ4BxyQiDKiDRfwzw0Jp1eQ0sW50jKcLUpESjhLGwYqGku+FH6wv7C2i`+
`gHf91iI1TkWwipHLxk3PTZSUCurIWrYx2Bc58+MZezP4d8zdntvLmMXex6GRV1QHADv5+QxYp78EW`+
`B9PmLkreprGXuUJAnUR22foI4FYvbnNDDiJXsBc5wDLDENyMqXbQN4PKElI0He9xj7qdibkbZlqbA`+
`Gj7kbEF74Ecyw2RjAsn8Gs8weJtBhhXRYJ6PWwtRemdooVvbTOheVxgE5kmbuTwDtMOsHgLopQ7TZ`+
`iS56pCYR0U3rKyTQsdSHtjdqcpPqkyRtLcRnby6Q+fTsDLDpB4K2bhKQE6Lpgnhufl9Qw75XK9MQW`+
`OGjZdUYsTYNhXMRWspItatXGLtW4ygskJliOawCoHQLAOM+ACXtQe10HZx41cfPT/VRHzvM6s/Ffz`+
`nM9Gf6mPr4vYQ237p1Byp+xaqvzfPlQWZYJuw3OBYDSZw8feGMeaox1L9uZ9Wrk+oVplq/BLsPTx+`+
`zK46ae1C/8bShjL9+O6nwgwBOqyNjxCvpFiyw4AT4nEvXYFUNHqcDJLfLQJ4yH8CKlgP2qhFgdt8v`+
`qW8YtXaiXvDbuFg1nZ+5Oi5pOjeWNwPYaEvWoPxxZxfBRq1I4tECsTItggCG87SfOgphQBMt2hDbN`+
`ncegQTKIxKp+m00j1Cg+LrHeNTSopjMQxpj+5t4aRGObXh1RBZKIfSxFI8+jOUO/1SLiRgibVq8BC`+
`ZVm/H4CYwX+vOYyoV+w8+qTSxfJWau3LRBoOi/oE9Bz+2VpPk2zddpfg8080zzg2BuClknqOgjZxz`+
`7dE9/kKSNqb/86egZg6AV/CkfMUejQF+r+V7ND2vc0S/z2aJhZvTZxHJpGPfhjNSMOqX5dVJr1kI0`+
`2NGHEY6ZqcUAohtbl8cFWMyrHjQPohLxmgHNawCQSvU5hgRrfAFWDwdtvarZALD6LxgrPtsB7DsHz`+
`nM7mmOM2Ru+JUwW1JumMDl8aswX0KrmgPTFEpkOVOjmH3G5H21yQnDKqIpH6LSfSAS8M1Js+iJ7aq`+
`4s0JumXDl7y6a9AkXuq1OT0hLThTcsFtS9UU5LSllNTeefFBdSL/jNpdal1BckN/7GzQ2A/gE3Qc0`+
`kNfOEKYM7cHNl7Ea+jpuwKvyg+u1UG8ICGsqq33jV3GNsXOwYOXyp6purWgtVr4uq19O61BiaOm/j`+
`wjRusbrx9wnTP3kqgfe+cQDd8a064BtfuIFbfBAA2IP3FDjhH+QQBBLo4kEr8JMPgjFyDwVwy00Eo`+
`hvfSwIr/6OEmLUHcDlfidgJ7rTGGYIEIhw2pReyyVZge2sUONkQqyppvIhfO0UWmO4tk2TSHrATXL`+
`NHpoRsRASSkw0AEdlclJzA4xXysqN1+oUSZGNGafAB2JtE7HQuCghciyJKT74oZysun2i8uDoEz4/`+
`sBfZoOyPv7/JgZGi/v3bCsxfV2x0N7d436IGSAEujdu38bfLI9RFbJrQe0r+53ECITo/NtNJBptnj`+
`5o+WI/Axw+qYbMpMT8+wyX/2v9c6b0vW+n3yfvlpu2uN5Dny7CVzFgtzb3/wYwtZCHRjJHDUNUb8N`+
`x2C8/EihrFzN/LwCLPw3QDKZyDWGV3mMwCjrsnSl3MXxM+X5ksJ82ZOnzpvmiwMGrX/6NGcnGM2Gq`+
`m+azmWM2qwzfvY4FEjBw3WOo5a1a9VI5wTr+FwxkoEcAi+I+EwlGbmgBZOPcYD3mwsxpmvDsK5sV1`+
`lxH0zuq+gmsumrI5ZF7ts4bJFyxYJ3jemzY6ZOWO6oH5uSjl76MjZzQJ9rDaynJn27YANPYXSyqYN`+
`Z77NPwPZxLH0+ZsWbpa3yJuS0lcIni/sDne7bpXd8cxuUErqW6JjY6NspSPNUagp2kE1gx+z37SCM`+
`F6zGXsKgNR4dhKe3Yv2gFiVLSch6Lm/DeHbEMqBQBvBz3HZmJGenmlzjDRnxqZHW8veDrQ4npW6q9`+
`tMc2VpUXy80Lq1rBgzsE/tYPKMtNOxdjts7hDhdKxua5Fizy/Ktyt2w4WSSpboOJTpnDk6DTazpAH`+
`sXlomcI2yjXB0MyFiVeuZNMG7DaCrzZlxWlMUNqmrBzo2QaruO/Fdxh7fh5NaPWUjZLnnW1hh0yKT`+
`cDsnXsLo4ovYuxhd/ATHBLWrRSmtPdCk7qNjLfnq2AEmz5ZM5y4cY+XFFOqWA8HTmK64QAYTnG+n9`+
`ezecTS9pDOc3bvnNsBEl7Ig970G3vK+zztoMqXh4P3IpMOAfVIC2CM9x4SUHrIaZJw5e8LE8OFDPu`+
`7XTW4vtDL3OjaowOqIMhUcO1Zgu2WWr/T7ccj34QcnZM1cL8g0KPUHI3ohyEkMiZB9kPHXEPsJtsM`+
`wJwgOpa8nONbHBkj8LUNSQKBdXaxqRKCFBpi+jF88T04Q1I/M8tykuavjVyXsHfftgNyhe3qtgFrq`+
`6rRtQHe3Hez4hFtgjX3fh8j0HN3rq92DER5Ngtm6DXJh8S1HLx+1ccAOYfzGL1ZHJMKJSClFb/krY`+
`ms0bwlYMJB+1wmTsPy1R8Fb2iAqnuoXD/Nder5i+57TuYe/zd/79aqVq1clrZXXChSEW7V45ZerBc`+
`+ca9n06+xrOrqOfm2ofnMd3W25qPrTJu/RtjL+NnlwkfovS4aDgTm5iVcAJj8nhKklkryY5+qdqb/`+
`a5IHaVsZf/6ad1PfA6ZTn/AJ7kA+6Kpu8CtIuv34gIr2eCFBdtx7EV6AyVReA7ehrQ8QsrfeH4PCh`+
`QyzUW6ZC1wJV30vVq0JX1VvGX+GnXlRfQPVU+AkGCOFuHXv27NjxTM/Ll8+cuXy555mONk/GMsHvk`+
`cF3wMMN3Q0SD74H2Isuoqttl8TbIIYIRYqze+jX/xgoVQwkkO8vZyLT57wtguH5LwCn2Xc4gmEA/B`+
`cSbIORezZDAOg7AvhcmiE6wZ4Mgh28TUIAg/t+hm0ZImdlw6xFARe4LRJEWXgKHPb0NsB2cLKIgOD`+
`jP9oATG8n4WCYYNsk4qL9vqItEtlm9pPcGEsUgXBza+CzOVhxPiaKMHJ4Aba1huHRxzjgbYANP604`+
`H7GDk/EOJENpa6G0nffDtK3+gNYzb4F4R3qKCAg+wgRnvInzkXec8QIWrZ4QJ1nnHP4oIgOJP9o8G`+
`9npRNr3vP20XScDMDjql/S1qJPVUPiZrE6m8JdOhp9Qyv/CE7TbAt2oRab6jodUg9BnpnHUjZYn/W`+
`X1bVk1qkb823Vbr2+HCwUFxsjt03Pkg7L94kW7fFI+OmHbMCEkzjg9LXrNFAgeYWGDBuWGHbPtkne`+
`mpewQaIwpRg1Qa6hmFcKTKqo2aqMi/PC/mn05TUQ/bIJmImhLdvK3xjsitxvlH3YjaQagPSIA85C0`+
`jjftZrOodbxu4zbHDcplNzXJP+xG+puNtPnPbXywRlZBW9GGq9t1WFsT0/c4whciweqqHSYVjQpvN`+
`DCyrAOsbkcXTKUOn8E7n2yY7dR+ER/BVspKp4tmrYu06eXsIl1a4Z2PYQG/82kFDW164p3PRwp/tE`+
`HXQgXvfFpxAmcXMrECwxkKWiBM6GRQfQ6QVX0bdPKsCKZ9NI9UTPtonoit2GVX+KPGwPRGF6laW+R`+
`MKqatPpdoDEnVOgrvsnr2/I3p27nXY6R3/ULc6F21Ad2QyPRTJq4EbHqhgS51FFrUyNIqeHuXFQv+`+
`fMAN8OyGTKg++udiNZJ/AVxh5tKbjHRvP4KxLONhRsJrLLRCaAWk6yxov7+CkYEHoLAcej6NX97V7`+
`QY8qu4BLGU98M3vDPXAkXOCGunAeW4OgSymVe5fjJ1cOw4S4+kQ6k6cxCBYFnmdX94tc13eTcjBci`+
`yl1+IB4yIHC9R/FVYMNz7AauI7xFIAGIYjdhPzx9ZnANbN+BASkfoNYesXQoC6deGgQPkCieCB5b8`+
`DqkjD9gsoVybM9uzSCFjguQ4NGfuVgj2cb36ZsYJP9llLYYHsjC2bsUuNihlpcTwG0r0jGKxJ1WYm`+
`5AE2VjVkFPCtCvm/p0e5QCfCAlO3bdu4B8o1pktaAla2MwKtjP0Ge0IIWAY7TlzYFQydz3iV8rQ+7`+
`PRfYKWs5APY4uefYNabuhdGBeMJqfsngBuHMf99WA5oKT4/fQE0j3tA7s0enkQmxdtAIpMeWv88Cp`+
`OYxwAnxylIsMmmPcB51Ah4vlINKOtdUvBCYytWhn/CcPYNgNIMrCnKHmKRcC8G6L0fwtDHFhDc90t`+
`M8E1VEFbqDF36UGg1DgWg/wkkJSc3wsxjxkkaxtauBkmJcRjMV9YUBlXqgjPBJMTkidHXdwlC74ci`+
`BxUN+howjB0A/gZ0NOwr5O8JZQSXmlz3F7WVkNRNWJfg+oi5LdHWTDw7IQ/3wXjfZGlHNP0Q71xR0`+
`xkxcE6oSeLXRdK0SwraoE5L8C6pCt4qPR0F7Cr7QvezYuBOmoH2GJsKz5P8wBeSkLPAOGotweXl4p`+
`ZhFPPELSmujZXNCWgrb4AqT/XEqukTAMu9QITkRziRYy1uyUloLCkFmj/zJL5tuHllD3C+7iJesKC`+
`tfCrh5iCnWtthzvAVQDR1AszWPQw371UGTKe/AINeotfTd0eefyI7DWIrqFzEkYpHA6AdDyO+SROR`+
`H4CUiViTkU8B1Vcl2sygdpFvA+6z+xCiSQmKlTTJQdlQGrLl1WF8JIZGXCOpsRu1gSsnuqtObRB3T`+
`GRQQ6RyMhE8Hb+A5xUq61Lsjtt2Q4pjkEVWbVDiXm2W81luePbk7Ek7YvdDlbUjPXtb9tbc7Jxvr9`+
`67SL1kahOou6p7FAS5eHndZEwePwes0SeYPO4ELOAlrHXHn98AvHJhCSh82FuQN568C44r5U78gvn`+
`4b8X85PgVX2yLyAiUg2RV10RW3QV66h7s5rsP5zP2c+vqcI77bQdb3xQAZqDsxhO+pwGWVpkwUc3A`+
`y4wE7d7NSKPi+xlpuzbsli8sOd5fDpH9+qr6Lm1Hj4wYO36qMH7a5HgZ/k1eNX7z+C0Ru0bv73K9L`+
`9XLL+ULx08o8jF579xN0wRPephKF+06WgUUUgY+LFTfnkr8D2/A/CHUojZVq0EY72qT1c4P1O40SH`+
`VLXrh28Up5pbxmWXISDaJuD2moTLsItKtajdZQm1qxGB0bdpuR/ps+ZWzvE/ByMXMSeDHaAq8sWSf`+
`EArAshcyaNc+S1VohakPV413V43EIXrPV1PoI+XGQNp6Q84V4kXG+Lxjhl/Nma7xJHy+BT2Xt7NZj`+
`4MAePZSBP/ygKD/8MFDpgUH4m+qw6floZ/0Q+24KmNGYkRh574MZHcTFIkbm3Ich+ceJF2j4FzCb/`+
`OsKkoguklF/J7mnIAlg+Tck71SF6dbBGdZvqULAud4rRB+2Bb3hSHRZNbgLK8RTxfC+QfO2B7nfVX`+
`gbWHH+dQKxdlkDaHgGzpll1oVZs3wBPAgHkLQBPfDR3yUIAuys4iLdKbmws3gBwh0l95Ea4I/3YjS`+
`/hm0S9202IGqJR4v0d9LjM8dAlx3fwYSgakMgWNYQhBx4T0JxYOSkCE0cLgkxe+8ScWdw9uuSCzuF`+
`/p9dF4UKry26vPYSwl235sk1dwttVpSjnqLRE9KG80Sx6kmaMJBMTPkNY1tD6B96A7CB94FTjVSgn`+
`xSOgz6si9c+uAT4J2rLcmKwTOHvs0rORxRH0wgXEbRk09SoqZXTc1Ujhht0DTOeZyjJlCu4Nz4gxN`+
`DfFb43KA7hkihmNAenUgOcu01IKAYDeJb/08S4c+wnPnOOpC2fsQvENTM9BJN0/AD9fXgtIOsbCGR`+
`7zgNIgSwZAHTvOcfBv9pgsIJk4hu0pG8Lrc3mjUx2Pcb6puA0kF3ENXNQcJqrg0Eqx7tIAFoZdpHg`+
`1pIVl4VWEtxa0bo0AF023mXWujQCTgoMrZ5MH4rvbrWacUEvSSskdaPWi876kOxToD7c/Dk/RrFc9`+
`4SfIASuTYEOAXKzBnBgCNGhhcdWWHgNvr2o7EofYtio0gi63F4CML3AbORtfpl4C56zDoMkym2YY2`+
`AXjD2mF0BfchsjbyOYSa2kuHho/LT04IGkzUl8F4MQ+uSreGq5qfkuFjVJ+SAuPY4UWPmk5xIvTi3`+
`aSstKn4Dq2PhkYBWfhsGzxIOLBqOeX+biglAvj6K/0CC2gkAiH4nBc/xyGDF7hGT0pCOK6Am77qXd`+
`8DLCgteFeFVopQscQ0A5tdIeM+I1MA0SkLahYKK5oZgtClcSIRQHD4blVf8YWBbCbgr1VgpqVdqRV`+
`qUdUBfv3uZtEHwLtsM4/xiko1+C7mqwzYw9yRoJPPPOQX4eVt1aulC7zbKPsDj0cnO1mpG/hkyAWf`+
`CG+yXPKCsg8FS01gSPBnfVaqV62egJxUmVXmAD2TlMt1MkRWAeqW82ACYRg2N4ib+lo9whsm9fxr7`+
`/8KUT3D4LESJ7RXvgn71aAKh8MRkzLTtgYwZAnGz/I2TOp3WCGqwGG+fPXzBblmTUlbxGXp+8YoVA`+
`g2kwrLpVa9jJyydaMPLB2p2YytYBbH0SLL3XOAfCiRAg326ZxUi7o+4cnDwJDXiX04u/aq2AIJOzt`+
`e/Jvic7bu8gC0Fux+X8mNyw3LBNcBhw4GsYw2FYWBjk/EhcgOCRSP41EOoohYQx0hgHwNBcbRgJ5h`+
`MDFLQ/fJwTg7G5ubnHN+XLgmcz+3G7YySEe6Eo315szy8y0DU02NLdzC+a6WBTBl40nzPfzjt+XaY`+
`WgVpUb+qntoAfb9VPtVhl1XK87e0hAr1L11uWNB0eEjJh5nxppjxdGFb3kFneKKXMXBGyY/iFJfeF`+
`+7dvUx3V3W7Z1KoeKu1oiYqNjY5Kj820wTRgpJkZeCMKAXqfou2u4xL8UfACYBa2DIKW3s5tTyxpZ`+
`+kt9546bJigpqqpxoQE2MJ5MnKTV+MWrhToRppqdG4GBw1RL56KhslvQEhzRecgbZdeYzgcKiNUdg`+
`XUhmvYbedIQT2lntJe7jjBc/7qXdEw+Q3ofMuDg4i79DcMh5MqfOcqoDZcw9ydQKAz6QzjihXJG2C`+
`xeDsNS5+1YH6CoM5QZ4AUdmTGwXOc6iqfugJiBkmcWG3XyDew+qKTEDH5DagR8pEIIEWlp0HDTSXy`+
`LSM+IjmNr4H55WUHvLy8j4M7ILMLSAHDPjvUa0tvMDpIHs4S7c5DN+1z0Xl5OGua5AoEYQWS88rQ8`+
`VDRBpJxXyuukLCsI4GQ0B+vfu5FYy3xuCbGhFrEGR3uRYNPLxs2DD0xDICTf9/ZT3wTIVTpLRGYHv`+
`gmci9+F3OSu660BxoFpjvfTcL3PZVt6GYLUesHsMSYCQOEQEzO2k8A4X4Zhr6argLaSlBFsr/wcsP`+
`YDka+WCJBCC+/ssXl5/lC2CBfYsLMbyqfEnMyDMMwJRccfOlayPHKl6QrrLzzPBBR9/EFKNDd7uPX`+
`MYHVMWZ+BgzPPRLRQbJjpXhADI3B3Sv2jUVe24qm2ym1g4xD3Ig3pV1LAkBDZb8MwM8APNvCAs11A`+
`Hv3Crrdq52xEkNDYL3bMvbHgAGwkh074+KnRsrjBLX9ZDP34N5qovrEKEd/NfqrWbDJdaLA+k/Vwe`+
`tdvAJpdRiOSH8VX4ie2YTwZgTedzReKguOvKXGrfKuHeu3Oj/WwS+ACPkOQKXmqHfPTEx99D4Yw0s`+
`xBrTkFfbnRHC9clH75JjkjTFZMatBvi0PoLr7zAdk3gV5Ptk5AsS1+ePKTE+wOL2bCnwoFFhlWx7I`+
`gjdVtwVZaFpPk/b+rHdpsAkqnymM3U24BTRT14ND/O0R7JYn+G2SjSzNCwFELJOttFCbv7S+yVvtE`+
`mhxdFS3m+KXT5etpYGmhIRVadxJvaU4TVSzW8xaInPRlu8q+PXZYkVr1PslKK5Lvbws2M2tT8Ec2K`+
`7KoPnDhRxgxlUZkqvyNqPwQy7sIl0CnP2kXzEBi/U8pLy22DuKZj7OvAMtVp/q4BbrK6NV1RQ1u9Y`+
`sjeccZX+GiHixCYdGdzsUrCy5B1jZ2wcJJB4PruMheogWqjbAkwL7wWhvsHNdRwFzqnc6AsNaZQhU`+
`CR/hEJW3iYAskfNiCUskF69r+bChd/C6oxa+boWhfyMv08hRg6e/cGoQciDi9AEa4D6g2iGXgsftF`+
`p2vDX7/QHR6g756rnTFwHShkLvpl69DrSpHYY1n9yoIJHzEy9z876BxzA0tLcOM/V4sekKtJqohun`+
`xGLCZmnaU3UkhSkUKKWson8ExWZvqMLQQ/K8AET8vmvR/i4AeKy4ksRt0dkd5IBSXkQ/6WCmJtBao`+
`DtOP7uLsf4HdLZ/H6qGUfEaUHLbNBPorLhWy/z8swGfhV9oMBf92C8c+mEdca6yoaKzMOIjgIgVXC`+
`R2SF/ax3K7zixevn8tTRhJXldpZMWBSvEJ2bsS4KvHLln2For2TNSXu53ubgFnSz4sD78Bh2SnFWy`+
`mGnXG1a7x3J+xgjhXvxgj7v/1z/XsRPthIn4oUz7oWW6WuJvwXtFXdM5nsqwzJCiIYbZdfW4feMWn`+
`37patw0ueOJyatQdsovmVQAWo5PeZK2gb3OEw03Cg76bU8/qHk5K5tHkgHxUwUbt7/Vt4J3jf+U4F`+
`n/d/ru/+TukDhmsLSgjWRzP8fkUSXSD8pFSKhcBkozUhSIdJF8b8SqZXy/xfpn1ri7JfiPWivFVgH`+
`d8OUA6YzOc8zOyu5KvYdaC3w/PoCQnp9bey8hfj3BURLyXVjMEDSeGKbwFcCJn9G5NOZXWK9vu84z`+
`ZVyna/pP1y1VEwovr570G48JNeNR2vljRsPPEralPiVi2vKv69wp/jGCsWKFVZM+K/Ljn+ssL/y7x`+
`WiQss9m0B1mjFDMoOLKMvWXC7T7TggVryaczkG/bAFFXGu1UB4/jRYsvw3J7d0FL/nRavhblE7t7x`+
`qXyw68yI4t+j6NH+rc7larfTW/K0IDPWGVOW/OLVlsWaJv+KzaEKzS5/BAS6/cBTGT50LfvHCESCS`+
`JbwOT/MRIcbV3gKCzPZBOd3whVdZHswpDBYxmQKuxlE4rXEUehFsJTq8LoSRUJhzWhJ/H0aU37ioo`+
`DIxeetkLyqiE4qK7LqtTgySNv1yvFxtZVFHqu/Dz0ib7J8dcCHkQsjD8KeyQNPRy6zMAI2U4NdcKw`+
`MxcfoLsBV7MDL5hIChVCd5sN4tZbD8L4IzY9KjbWq6SZQ7bR+YP0CJLJR/lWkg9YKfQEALI5UB+QO`+
`3d5JF4X2zLMsLFs1ftGBRwvglsjwvcd4KKXnOavkbYb+ZkqAilagkKAhhURAl1kPygdV705OXJycl`+
`L18Zvyph1QJGbJXrgsIvzoAqc+S3IFuTNZDqtf0Qd+TmLYRxbrh7z2R5m5y7dWMWyPxWUBDkBDU34`+
`J3uMMDuF6Mira8w0CbvAB5lUwDreU62XjXLT8Mfgi4uBGT7gzJIaTeLPH9RQqwUJ42P/0wWhg09fO`+
`rUYfxvaB+bZ3M7DbEz3fqGWbG6A/Yn+HnLE7vBUQe0C5tWtAi41ryGGOZ8HvixrB2/nXj5zgKEI9u`+
`AZt2jIeE/b+dAUE+oacb4+AVz5TlC6XtmnlQ6nmlfYTneM8vrFqyNXyWox2maMWHFvOWQ/C+pb6Fr`+
`1DhZfdxYlmmU80OhmfhJD2xKVGP62Ez7qO9baD01RK2HIfjCR8R5vqZ9DvZ17x1+vi6LrvOVoOVDW`+
`P80h+eF+/F8fYP2eOIEcYJv7kA2UD4gFJ0EdpH2HbALcjLSvTZ/OZyGB6k7vptt3xH7O2pdNjzV6K`+
`G71+YEopMUmFhZ2Y9/AOp7nZ8YEKP8gDf62bspeKKf4zF7pvBHfA0cpvAubH2Oh68qgJrh/PX3lWa`+
`KM8VrNYj8PffDnLlWmjMHIc9MFZggOl/4k25ekE/M/pnnE9E4sy+CmhJ/tPGKCVMF9F3e90StKCS+`+
`S0EoXZ3pIM6ZLAAHb2OSeq8RyPsoE51khOR8vOcPw6sPdbYR38vAEzaBF4qXeRcy3iA5H33xBu5Rp`+
`qi1ITWkYQdvKdradHVm8kQjUXIVmPh6AB5Bz0fm4P94wYs+lF4TFhJ1UN6GteiZakE807cpVpwZ2i`+
`l3Ec3jL7xn5rlvN6uk2YSuzgzFVXCEHZWcVYjTO3NDaQGp8aiGmspwZTXRt/pylUk2cEnpxTD3Afz`+
`A4GRngvcXqFiaBM8lVwC8aMcvLebgnTOUDua/la5cvYlYnDizqkRnF6xI67Jx4bAsqQJ+lZXg/6Hi`+
`8T3MUIxhWJiEbA98D41tf4R5dCNUfGmKOuOCAhsuKBhguT+mH1rxgssrL1ogopmjqRU1E50fLujsk`+
`eiI22Mq8mMJpg3376HHnSXK+I15dcUIfmHHrV12uoYOVgcXedGa9lX247dogN2bSnRbSaglaklcrB`+
`wthJo/mRk2SO6NZ73T6T6FVnrcVHjqVCGede8/pMI+pzpZ1aVntE/3gDZBHWyBdhiVmYZf7I0trU3`+
`HmjPT0r+yDlf/sDhq+5mi4uKirOpx05S4NLzwKrREx8VFR6fFZWSkpWVkxKVF2zzb2mmknebZdbR1`+
`ET1a5PxSx6eun1rDJvfa0i8nTPX6qK3qI6sNtO9AS9po13MybdCW+qjVzmbnbvlOLhBojbovVR9ro`+
`Nvv57q3bdO9e5s23c/9fv3cud9td0rdLMVjLzdNVXVC6WVTKtVdvv9y19a5WQt2yjvlrORtawXPkv`+
`3VX1WzqE2of8l7a9YVmazuJAH8tEeljR7uRe6/rX6YtHJ30o51jpRneR4eRZWLPKr8D07klaoAAAA`+
`BAAAAANXtRbgAAAAA2+7/5QAAAADb8JBDeJxj/MLAwPyCwRCI1YA4hfnF/89AWhGIvYFYDIglgZgN`+
`ytaFqgPJG0H5RswKDBwMnAzSDPoMXEAWAxALgmkBhkoGSYZWhmwGDYYSBkuGRKAaNbAcB5ClAySlg`+
`LiVQQgAj/wY8g==) format('woff');
	font-weight: normal;
	font-style: normal;
}
.radar {
	width: `+pvars.settings.width+`px;
	height: `+pvars.settings.height+`px;
	position: relative;
}
.radar-title {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: normal;
	color: `+pvars.settings.highlight_color+`;
	position: absolute;
	top: 0;
	left: 0;
	padding: 0.25cm;
	background-color: `+pvars.settings.background_color+`;
	border-radius: 0 0 0.375cm 0;
}
.radar-searchbox-container {
	height: 0.75cm;
	margin: 0.25cm;
	padding-left: 0;
	padding-right: 0;
	border: 1px solid `+pvars.settings.searchbox_border_color+`;
	width: 100%;
	background-color: `+pvars.settings.searchbox_background_color+`;
	border-radius: 0.375cm;
	outline: none;
	overflow: auto;
}
.radar-searchbox-icon {
	font-size: 0.65cm;
	color: `+pvars.settings.searchbox_icon_color+`;
	height: 0.75cm;
	padding-left: 0.2cm;
	padding-right: 0.15cm;
	width: 0.5cm;
	background-color: #0000;
	outline: none;
}
.radar-searchbox-input {
	height: 0.75cm;
	padding-right: 0.25cm;
	border: 0;
	width: 100%;
	background-color: #0000;
	color: `+pvars.settings.searchbox_text_color+`;
	outline: none;
}
.radar-views-box {
	position: absolute;
	top: 1.5cm;
	left: 0.25cm;
	display: inline-block;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 0.8em;
	padding: 0;
	border-radius: 0.175cm;
	border: 1px solid `+pvars.settings.viewsbox_border_color+`;
	background-color: `+pvars.settings.viewsbox_background_color+`;
	overflow: auto;
	z-index: 3;
}
.radar-views-box a {
	color: `+pvars.settings.viewsbox_text_color+`;
	padding: 0.125cm 0.5cm 0.125cm 0.4cm;
	text-decoration: none;
	display: block;
	cursor: pointer;
}
.radar-views-box a:hover {
	background-color: `+pvars.settings.viewsbox_background_color_hover+`;
}
.radar-views-box .radar-icon {
	font-size: 1.25em;
}
.radar-views-box-name {
	color: green;
}
.radar-views-box-name-selected {
	font-weight: bold;
}
.radar-legend-box {
	position: absolute;
	bottom: 0.25cm;
	left: 0.25cm;
	display: inline-block;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1em;
	padding: 0;
	border-radius: 0.175cm;
	border: 1px solid `+pvars.settings.legend_border_color+`;
	background-color: `+pvars.settings.legend_background_color+`;
	overflow: auto;
	z-index: 3;
}
.radar-legend-box span {
	color: `+pvars.settings.legend_text_color+`;
	margin: 0.125cm 0.5cm 0.125cm 0.4cm;
	text-decoration: none;
	display: block;
	font-size: 0.8em;
}
.radar-legend-box .radar-icon {
	font-size: 0.75em;
}
.radar-icon {
	font-family: radaricons;
	display: inline-block;
	width: 30px;
	line-height: 27px;
}
.radar-show {display: block;}
.radar-sidebox {
	width: 8cm;
	background-color: #0000;
	box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.0);
	transition: background-color 0.3s ease;
	overflow: hidden;
}
.radar-sidebox-open {
	background-color: `+pvars.settings.sidebox_background_color+`;
	box-shadow: 0px 0px 16px 0px rgba(0,0,0,0.2);
}
.radar-description-list {
	margin:0.25cm;
	position: absolute;
	top: 1.25cm;
	bottom: 0;
	left:0;
	right: 0;
	overflow-y: scroll;
}
.radar-description-list h1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.2em;
	color: `+pvars.settings.sidebox_h_1_color+`;
}
.radar-description-list h2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1em;
	color: `+pvars.settings.sidebox_h_2_color+`;
	padding: 0.075cm;
	padding-left: 0.25cm;
	border-radius: 0.175cm;
	cursor: pointer;
}
.radar-description-list h2:hover {
	background-color: `+pvars.settings.sidebox_background_color_hover+`;
}
.radar-description-box {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 0.8em;
	margin: 0.25cm;
}
.radar-description-text {
	font-size: 1.0em;
	color: `+pvars.settings.description_text_color+`;
	margin-left: 0.5cm;
}
.radar-description-text a {
	text-decoration: none;
	color: `+pvars.settings.link_color+`;
}
`
		var styleSheet = document.createElement("style");
		styleSheet.type = "text/css";
		styleSheet.innerText = rcss;
		document.head.appendChild(styleSheet);
	}


	Radar.getById = function(id){
		return (id in $StaticPrivate.idDict) ? $StaticPrivate.idDict[id] : null;
	}
	
	var initCalled = false;
	Radar.Init = function(){
		if (initCalled) return;
		initCalled = true;
		$StaticPrivate.onload();
		window.requestAnimationFrame($StaticPrivate.renderAll);
	}

//	document.addEventListener('DOMContentLoaded', function() {
//
//		$StaticPrivate.onload();
//
//		window.requestAnimationFrame($StaticPrivate.renderAll);
//
//	}, false);

	return Radar;
}();

// CODE FROM: https://riptutorial.com/html5-canvas/example/18742/rendering-text-along-an-arc-
(function(){
    const FILL = 0;        // const to indicate filltext render
    const STROKE = 1;
    var renderType = FILL; // used internal to set fill or stroke text
    const multiplyCurrentTransform = true; // if true Use current transform when rendering
                                           // if false use absolute coordinates which is a little quicker
                                           // after render the currentTransform is restored to default transform



    // measure circle text
    // ctx: canvas context
    // text: string of text to measure
    // r: radius in pixels
    //
    // returns the size metrics of the text
    //
    // width: Pixel width of text
    // angularWidth : angular width of text in radians
    // pixelAngularSize : angular width of a pixel in radians
    var measure = function(ctx, text, radius){
        var textWidth = ctx.measureText(text).width; // get the width of all the text
        return {
            width               : textWidth,
            angularWidth        : (1 / radius) * textWidth,
            pixelAngularSize    : 1 / radius
        };
    }

    // displays text along a circle
    // ctx: canvas context
    // text: string of text to measure
    // x,y: position of circle center
    // r: radius of circle in pixels
    // start: angle in radians to start.
    // [end]: optional. If included text align is ignored and the text is
    //        scaled to fit between start and end;
    // [forward]: optional default true. if true text direction is forwards, if false  direction is backward
    var circleText = function (ctx, text, x, y, radius, start, end, forward) {
        var i, textWidth, pA, pAS, a, aw, wScale, aligned, dir, fontSize;
        if(text.trim() === "" || ctx.globalAlpha === 0){ // dont render empty string or transparent
            return;
        }
        if(isNaN(x) || isNaN(y) || isNaN(radius) || isNaN(start) || (end !== undefined && end !== null && isNaN(end))){ //
            throw TypeError("circle text arguments requires a number for x,y, radius, start, and end.")
        }
        aligned = ctx.textAlign;        // save the current textAlign so that it can be restored at end
        dir = forward ? 1 : forward === false ? -1 : 1;  // set dir if not true or false set forward as true
        pAS = 1/radius;                 // get the angular size of a pixel in radians
        textWidth = ctx.measureText(text).width; // get the width of all the text
        if (end !== undefined && end !== null) { // if end is supplied then fit text between start and end
            pA = ((end - start) / textWidth) * dir;
            wScale = (pA / pAS) * dir;
        } else {                 // if no end is supplied correct start and end for alignment
            // if forward is not given then swap top of circle text to read the correct direction
            if(forward === null || forward === undefined){
                if(((start % (Math.PI * 2)) + Math.PI * 2) % (Math.PI * 2) > Math.PI){
                    dir = -1;
                }
            }
            pA = -pAS * dir ;
            wScale = -1 * dir;
            switch (aligned) {
            case "center":       // if centered move around half width
                start -= (pA * textWidth )/2;
                end = start + pA * textWidth;
                break;
            case "right":// intentionally falls through to case "end"
            case "end":
                end = start;
                start -= pA * textWidth;
                break;
            case "left":  // intentionally falls through to case "start"
            case "start":
                end = start + pA * textWidth;
            }
        }

        ctx.textAlign = "center";                     // align for rendering
        a = start;                                    // set the start angle
        for (var i = 0; i < text.length; i += 1) {    // for each character
            aw = ctx.measureText(text[i]).width * pA; // get the angular width of the text
            var xDx = Math.cos(a + aw / 2);           // get the yAxies vector from the center x,y out
            var xDy = Math.sin(a + aw / 2);
            if(multiplyCurrentTransform){ // transform multiplying current transform
                ctx.save();
                if (xDy < 0) { // is the text upside down. If it is flip it
                    ctx.transform(-xDy * wScale, xDx * wScale, -xDx, -xDy, xDx * radius + x, xDy * radius + y);
                } else {
                    ctx.transform(-xDy * wScale, xDx * wScale, xDx, xDy, xDx * radius + x, xDy * radius + y);
                }
            }else{
                if (xDy < 0) { // is the text upside down. If it is flip it
                    ctx.setTransform(-xDy * wScale, xDx * wScale, -xDx, -xDy, xDx * radius + x, xDy * radius + y);
                } else {
                    ctx.setTransform(-xDy * wScale, xDx * wScale, xDx, xDy, xDx * radius + x, xDy * radius + y);
                }
            }
            if(renderType === FILL){
                ctx.fillText(text[i], 0, 0);    // render the character
            }else{
                ctx.strokeText(text[i], 0, 0);  // render the character
            }
            if(multiplyCurrentTransform){  // restore current transform
                ctx.restore();
            }
            a += aw;                     // step to the next angle
        }
        // all done clean up.
        if(!multiplyCurrentTransform){
            ctx.setTransform(1, 0, 0, 1, 0, 0); // restore the transform
        }
        ctx.textAlign = aligned;            // restore the text alignment
    }
    // define fill text
    var fillCircleText = function(text, x, y, radius, start, end, forward){
        renderType = FILL;
        circleText(this, text, x, y, radius, start, end, forward);
    }
    // define stroke text
    var strokeCircleText = function(text, x, y, radius, start, end, forward){
        renderType = STROKE;
        circleText(this, text, x, y, radius, start, end, forward);
    }
    // define measure text
    var measureCircleTextExt = function(text,radius){
        return measure(this, text, radius);
    }
    // set the prototypes
    CanvasRenderingContext2D.prototype.fillCircleText = fillCircleText;
    CanvasRenderingContext2D.prototype.strokeCircleText = strokeCircleText;
    CanvasRenderingContext2D.prototype.measureCircleText = measureCircleTextExt;
})();


